This is a small JEE app (Pinkish Car Rental). 


CONFIG
======

Before building the app: 

0. Create MySql DB `carsrental` and run `scripts/createdb.sql` 

1. optionally fill `app.properties` in `./properties` 

2. optionally fill glassfish `<properties>...</properties>` in `pom.xml` 


Before running the app on server: 

Given your Glassfish dir on server is something like `${GLASSFISH_DIR}=.../glassfish5/glassfish`: 

1. create `passfile.txt` in Glassfish's config directory: 

       cp ./properties/passfile.txt_sample ${GLASSFISH_DIR}/config/passfile.txt 
       
   and fill it for user `admin`, 
    
2. create file `email.properties` in Glassfish's config directory: 
    
       cp ./properties/email.properties_sample ${GLASSFISH_DIR}/config/email.properties 
       
   and fill its properties in. 

NOTE: in case you are using Windows: remove `asadmin` from your `${GLASSFISH_DIR}/bin`, leave only `asadmin.bat` 



BUILD AND DEPLOY LOCALLY
========================

With only unit tests: 

    mvn clean install
    
With integration tests (DB related tests): 
    
    mvn clean install -Pintegration



TEST THE APP
============

Open in browser:

    http://localhost:8080/jeesmallapp-hi/

Test users: 

    admin / "aa"
    anka / ""

    
CURRENT STATE OF AFFAIRS
========================

Registration, Login, sending Activation Email, Activating Account and Deleting Account work. 
Displaying cars works. 
Editing/Deleting user account by Admin are added. 

TODOs: 
 * DateHelper unit tests TODO 
 * tests for CarsDAO are a TODO 
 * tests for Rental TODO 
 * and refactoring a little Editing user account 
 * prep separate module for integration tests and e2e tests (with starting DB & Glassfish > run of only marked tests) 
 