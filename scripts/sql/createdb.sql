create table car
(
    car_id          int auto_increment
        primary key,
    color_hex       varchar(6)  null,
    seats           int         null,
    brand           varchar(50) null,
    production_year int         null,
    horse_power     int         null,
    price_per_day   double      null
);

create table user
(
    username        varchar(50)  not null
        primary key,
    password        varchar(50)  not null,
    email           varchar(50)  null,
    activation_code varchar(50)  null,
    activation_path varchar(250) null,
    activated       tinyint(1)   null,
    role            varchar(5)   null
);

create table rental
(
    rental_id     int auto_increment
        primary key,
    username      varchar(50)  not null,
    car_id        int          not null,
    date_started  date         not null,
    days_to_rent  int          null,
    date_returned timestamp    null,
    comment       varchar(255) null,
    price_paid    double       null,
    price_due     double       null,
    price_per_day double       null,
    constraint rental_car_id_fk
        foreign key (car_id) references car (car_id),
    constraint rental_ibfk_1
        foreign key (username) references user (username)
);

create index user_id
    on rental (username);

