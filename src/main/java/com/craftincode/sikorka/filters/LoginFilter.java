package com.craftincode.sikorka.filters;

import com.craftincode.sikorka.servlets.home.ActivateEmailServlet;
import com.craftincode.sikorka.servlets.LoginServlet;
import com.craftincode.sikorka.servlets.RegisterServlet;
import com.craftincode.sikorka.servlets.TestDbServlet;
import com.craftincode.sikorka.util.HttpHelper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginFilter extends HttpFilter {

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {

        Object username = req.getSession().getAttribute(LoginServlet.USERNAME_FORM_PARAM);

        if (username != null) { //logged in
            chain.doFilter(req, res);

            return;
        }

        if (req.getRequestURI().contains(LoginServlet.MY_JSP) ||
                req.getRequestURI().contains(LoginServlet.MY_PATH) || //if I am on login (entry) page
                req.getRequestURI().contains(RegisterServlet.MY_JSP) ||
                req.getRequestURI().contains(RegisterServlet.MY_PATH) || //if I am on registration page
                req.getRequestURI().contains(".css") || //or I am a style
                req.getRequestURI().contains(ActivateEmailServlet.MY_PATH) ||
                req.getRequestURI().contains(TestDbServlet.MY_PATH)
        ) {
            chain.doFilter(req, res);

            return;
        }

        HttpHelper.sendRedirectNOcontextPath(LoginServlet.getMyJSP(), 400, res, req);
    }

}
