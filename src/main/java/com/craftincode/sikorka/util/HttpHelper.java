package com.craftincode.sikorka.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HttpHelper {

//    public static void sendRedirectWithContextPath(String url, int status, HttpServletResponse resp, HttpServletRequest req) throws IOException {
//        resp.setStatus(status);
//
//        String redirectURL = req.getContextPath() + "/" + resp.encodeRedirectURL(url);
//        System.out.println("Redirect URL: " + redirectURL);
//
//        resp.sendRedirect(redirectURL);
//    }

    public static void sendRedirectNOcontextPath(String url, int status, HttpServletResponse resp, HttpServletRequest req) throws IOException {
        resp.setStatus(status);

        String redirectURL = resp.encodeRedirectURL(url);
        System.out.println("Redirecting to URL: " + redirectURL + " (from " + url + ")");

        resp.sendRedirect(redirectURL);
    }

    public static void sendRedirectNOcontextPath(String url, int status, HttpServletResponse resp, HttpServletRequest req, String... parameters) throws IOException {
        if (parameters == null || parameters.length <= 0) {
            sendRedirectNOcontextPath(url, status, resp, req);
            return;
        }

        if (parameters.length == 1) {
            sendRedirectNOcontextPath(url + "?" + parameters[0], status, resp, req);
            return;
        }

        String params = parameters[0];
        for (int i=1; i<parameters.length; i++)
            params = params + "&" + parameters[i];

        sendRedirectNOcontextPath(url + "?" + params, status, resp, req);
    }

    public static void sendRedirectNOcontextPath(String url, HttpServletResponse resp, HttpServletRequest req, String... parameters) throws IOException {
        sendRedirectNOcontextPath(url, resp.getStatus(), resp, req, parameters);
    }

}
