package com.craftincode.sikorka.util;

import static com.craftincode.sikorka.util.Log.*;

import java.sql.*;

public class DbHelper {

    static {
        Log.init();
    }

    public static Connection connect() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            error("Initializing DB driver failed :(");

            return null;
        }

        try {
//            say("Connecting to DB...");

            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/carsrental?" +
                            "autoReconnect=true&" +
                            "useSSL=false&" +
                            "serverTimezone=UTC&" +
                            "allowPublicKeyRetrieval=true",
                    "root",
                    "rootroot");

//            ok("Connected!");

            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
            error("Could not connect to DB!");

            return null;
        }
    }

    public static boolean close(Connection connection) {
        if (connection == null)
            return false;

        try {
//            say("Disconnecting from DB...");
            connection.close();
//            ok("DB disconnected.");

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            error("Could not disconnect from DB!");

            return false;
        }
    }

    public static boolean close(PreparedStatement ps) {
        if (ps == null)
            return false;

        try {
//            say("Closing prepped statement...");
            ps.close();
//            ok("Prepped statement closed.");

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            error("Could not close prepped statement!");

            return false;
        }
    }

    public static boolean close(ResultSet rs) {
        if (rs == null)
            return false;

        try {
//            say("Closing result set...");
            rs.close();
//            ok("Result set closed.");

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            error("Could not close result set!");

            return false;
        }
    }

    /** When using similar query multiple times definitely use <code>execute(PreparedStatement ps)</code> (better for performance). */
    public static ResultSet executeModify(Connection connection, String sqlQuery) {
        if (connection == null)
            return null;

        try {
            say("Executing the SQL query: " + sqlQuery);
            ResultSet rs = connection.createStatement().executeQuery(sqlQuery);
            ok("SQL query executed!");

            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            error("SQL query execution failed.");

            return null;
        }
    }

    public static int executeModify(PreparedStatement ps) {
        if (ps == null)
            return 0;

        try {
            say("Executing the modifying prepped statement: " + ps);
            int amountAltered = ps.executeUpdate();
            ok("Prepped statement executed. Rows altered: " + amountAltered);

            return amountAltered;
        } catch (SQLException e) {
            e.printStackTrace();
            error("Modifying prepped statement execution failed.");

            return 0;
        }
    }

    public static boolean executeSelect(PreparedStatement psSelect) {
        if (psSelect == null)
            return false;

        try {
            say("Executing the select prepped statement: " + psSelect);
            boolean anythingSelected = psSelect.execute();
            ok("Rows successfully selected. ");

            return anythingSelected;
        } catch (SQLException e) {
            e.printStackTrace();
            error("Select prepped statement execution failed.");

            return false;
        }
    }

    public static PreparedStatement prepareStatement(Connection connection, String s) {
        if (connection == null)
            return null;

        try {
//            say("Prepping statement: " + s);
            PreparedStatement ps = connection.prepareStatement(s);
//            ok("Statement prepared!");

            return ps;
        } catch (SQLException e) {
            e.printStackTrace();
            error("Could not prepare statement!");

            return null;
        }
    }

    private static void say(String m) {
        if (Log.debugDB)
            Log.say(m);
    }

    private static void ok(String m) {
        if (Log.debugDB)
            Log.ok(m);
    }

}
