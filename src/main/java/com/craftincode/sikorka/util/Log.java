package com.craftincode.sikorka.util;

import com.craftincode.sikorka.AppHelper;
import com.github.sikorka.Outfit;
import com.github.sikorka.TinyLog;
import com.github.sikorka.tinylog.Color;
import com.github.sikorka.tinylog.Font;
import com.github.sikorka.tinylog.SpaceOut;

import static com.github.sikorka.TinyLog.*;

public class Log {

    public static final boolean debug = AppHelper.DEBUG_ON;
    public static final boolean debugDB = true;
    public static final boolean debugMAIL = true;

    static {
        Outfit bold = new Outfit()
                .sayColor(Color.YELLOW)
                .loudColor(Color.GREEN_BOLD_INTENSE)

                .highlightColor(Color.RED_BOLD)
                .highlightFont(Font.NONE)

                .shoutColor(Color.BOLD_PURPLE)
                .shoutFont(Font.NONE)

                .setSpaceOut(SpaceOut.ONE_LINE);

        bold.applyLook();

        TinyLog.say("Saying sth");
        sayLoud("Saying sth loud");
        highlight("Highlighting sth");
        shout("Shouting sth");
    }

    /** Just for forcing static block to execute and initialize log colors. */
    public static void init() { }

    public static void say(String msg) {
        if (debug)
            TinyLog.say(msg);
    }

    public static void ok(String msg) {
        if (debug)
            sayLoud(msg);
    }

    public static void critical(String msg) {
        shout(msg);
    }

    public static void error(String msg) {
        highlight(msg);
    }

}
