package com.craftincode.sikorka.util;

import java.awt.*;
import static com.craftincode.sikorka.util.Log.*;

/** Various helpful things. */
public class Util {

    public static Color hexToColor(String colorInHex) {
        if (colorInHex == null || "".equals(colorInHex))
            return null;

        try {
            say("Hex to Color from: " + colorInHex);

            colorInHex = colorInHex.replace("#", "");
            colorInHex = String.
                    format("%1$-6s", colorInHex). //add remaining ' ' until 6 length
                    replace(' ', '0'); //'0' for each space

            return new Color(
                    Integer.valueOf(colorInHex.substring(0, 2), 16),
                    Integer.valueOf(colorInHex.substring(2, 4), 16),
                    Integer.valueOf(colorInHex.substring(4, 6), 16));
        } catch (Exception e) {
            e.printStackTrace();
            error("Can't transform " + colorInHex + " to Hex. ");
        }

        return null;
    }

    public static String colorToHex(Color color) {
        if (color == null)
            return null;

        return String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
    }
}
