package com.craftincode.sikorka.util;

import com.craftincode.sikorka.AppHelper;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Date;
import java.util.Properties;
import static com.craftincode.sikorka.util.Log.*;

public class EmailHelper {
    private static final String FROM_EMAIL = AppHelper.getEmailProperty("FROM_EMAIL");
    private static final String FROM_PASS = AppHelper.getEmailProperty("FROM_PASS");
    private static final String SMTP_HOST = AppHelper.getEmailProperty("SMTP_HOST");

    private static MimeMessage composeEmail(String to, String content, String subject) {

        //prep the session object
        Properties props = System.getProperties();
        props.put("mail.smtp.host", SMTP_HOST);
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.transport.protocol", "smtp");

        props.put("mail.debug", AppHelper.DEBUG_ON);
        props.put("mail.smtp.socketFactory.fallback", "false");

        props.put("mail.smtp.ssl.checkserveridentity", "false");
        props.put("mail.smtp.ssl.trust", "*");
        props.put("mail.smtp.connectiontimeout", "10000");

        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(FROM_EMAIL, FROM_PASS);
            }
        };
        Session session = Session.getInstance(props, auth);

        //compose the message
        try{
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(FROM_EMAIL));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setContent(content, "text/html");
            message.setSentDate(new Date());

            return message;

        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

        return null;
    }

    private static boolean sendEmail(MimeMessage mimeMessage) {
        //no data for email message
        if (mimeMessage == null)
            return false;

        try {
            //send message and pray
            say("Sending message...");
            Transport.send(mimeMessage);
            ok("Message sent successfully!");

            return true;
        } catch (MessagingException e) {
            e.printStackTrace();

            error("Messaging exception: " + e.getMessage());
        }

        error("Message not sent.");

        return false;
    }

    public static void sendEmail(String to, String content, String subject) {
        say("Sending email to: " + to + ", " +
                "subject: " + subject + ", " +
                "content: " + content);

        MimeMessage myEmail = composeEmail(to, content, subject);

        if (myEmail == null)
            return;

        if (sendEmail(myEmail))
            ok("Email sent.");
        else
            error("Email not sent.");
    }

    public static void main(String [] args) {
        sendEmail("ania@sikorka.eu", "test", "test msg");
    }

    /** Used for tests. */
    public static void readEmail(String to, String subject, String content) {
        //TODO
    }

}
