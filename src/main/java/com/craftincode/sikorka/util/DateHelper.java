package com.craftincode.sikorka.util;

import org.apache.commons.lang3.time.DateUtils;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import static com.craftincode.sikorka.util.Log.*;

public class DateHelper {
    public static String pattern = "yyyy-MM-dd";

    private static SimpleDateFormat format = new SimpleDateFormat(pattern);

    public static Date datePlusDays(Date dateStarted, int daysToRent) {
        LocalDate dateStart = dateStarted.toLocalDate();
        LocalDate dateEnd = dateStart.plus(daysToRent, ChronoUnit.DAYS);

        say("Date " + dateStarted + " + " + daysToRent + " days = " + dateEnd);
        return Date.valueOf(dateEnd);
    }

    public static Date datePlusDays(java.util.Date dateStarted, int daysToRent) {
        return datePlusDays(getDateNoTime(dateStarted), daysToRent);
    }

    public static java.sql.Date todayDate() {
        java.sql.Date todayDate = getDateNoTime(new java.util.Date());

        say("Today date is: " + todayDate);
        return todayDate;
    }

    public static java.sql.Date nowDate() {
        return todayDate();
    }

    public static java.util.Date tomorrowDate() {
        return datePlusDays(todayDate(), 1);
    }

    public static String today() {
        return format.format(todayDate());
    }

    public static String tomorrow() {
        return format.format(tomorrowDate());
    }

    public static java.sql.Date stringToDate(String dateString) {
        if (dateString == null || "".equals(dateString))
            return null;

        try {
            return new java.sql.Date(format.parse(dateString).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean isBeforeDay(java.sql.Date date1, java.sql.Date date2) {
        java.util.Date date1withoutTime = getDateNoTime(date1);
        java.util.Date date2withoutTime = getDateNoTime(date2);

        boolean date1beforeDate2 = date1withoutTime.before(date2withoutTime);
        say("Date 1: " + date1 + " before Date 2: " + date2 + " is: " + date1beforeDate2);

        return date1beforeDate2;
    }

    public static Date getDateNoTime(java.util.Date dateToReturn) {
//        Date dateNoTime = new Date(dateToReturn.getTime()); //this does not work, java.sql.Date still has time
        Date dateNoTime = new Date(DateUtils.truncate(dateToReturn, Calendar.DATE).getTime());

        say("Removing time from date: " + dateNoTime);
        return dateNoTime;
    }

    public static boolean isAfterDay(java.util.Date date1, java.util.Date date2) {
        java.util.Date date1withoutTime = getDateNoTime(date1);
        java.util.Date date2withoutTime = getDateNoTime(date2);

        return date1withoutTime.after(date2withoutTime);
    }

    public static boolean isSameDay(java.util.Date date1, java.util.Date date2) {
        boolean isIt = DateUtils.isSameDay(date1, date2);

        say("Is same day calculation: " + date1 + " and " + date2 + " = " + isIt);
        return isIt;
    }

    public static Integer daysDifferenceToNow(Date dateToReturn) {
        if (dateToReturn == null)
            return null;

        LocalDate now = LocalDate.now();
        int days = Period.between(dateToReturn.toLocalDate(), now).getDays();

        say("Days difference calculation: " + dateToReturn + " and " + now + " = " + days);
        return days;
    }

}
