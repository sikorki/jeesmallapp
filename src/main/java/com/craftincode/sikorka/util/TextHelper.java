package com.craftincode.sikorka.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TextHelper {
    public static Gson gson;

    public static String toJsonString(Object o) {
        return getGson().toJson(o);
    }

    public static boolean equalsIgnoreCase(String string1, String string2) {
        if (string1 == null) {
            return string2 == null;
        } else {
            return string1.equalsIgnoreCase(string2);
        }
    }

    private static Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
//                    .setPrettyPrinting()
                    .serializeNulls()
                    .disableHtmlEscaping()
                    .create();
        }

        return gson;
    }
}
