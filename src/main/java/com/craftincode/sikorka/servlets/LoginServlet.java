package com.craftincode.sikorka.servlets;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.home.HomePageServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/" + LoginServlet.MY_PATH)
public class LoginServlet extends BaseServlet {
    public static final String MY_PATH = "login";
    public static final String MY_JSP = "index.jsp";

    public static final String USERNAME_FORM_PARAM = "username";
    public static final String PASSWORD_FORM_PARAM = "password";

    public static final String USER_DOES_NOT_EXIST_URL_PARAM = "userDoesNotExist";
    public static final String USER_DOES_NOT_EXIST_MSG = "User does not exist!";

    public static final String BAD_PASS_URL_PARAM = "badPass";
    public static final String BAD_PASS_MSG = "U got bad pass there!";

    public static final String USER_NOT_LOGGED_IN_NO_SESSION_URL_PARAM = "userNotLoggedInNoSession";
    public static final String USER_NOT_LOGGED_IN_NO_SESSION_MSG = "User not logged in.";

    public static final String USER_NOT_LOGGED_IN_SESSION_URL_PARAM = "userNotLoggedInSession";
    public static final String USER_NOT_LOGGED_IN_SESSION_MSG = "User not logged in! (There is session but no username in it.)";

    public static final String NO_USERNAME_PROVIDED_URL_PARAM = "noUsernameProvided";
    public static final String NO_USERNAME_PROVIDED_MSG = "No " + USERNAME_FORM_PARAM + " param provided!";

    public static final String NO_PASS_PROVIDED_URL_PARAM = "noPassProvided";
    public static final String NO_PASS_PROVIDED_MSG = "No " + PASSWORD_FORM_PARAM + " param provided!";

    public static final String BAD_DATA_URL_PARAM = "badData";
    public static final String BAD_DATA_MSG = "We have some bad data here... Aborting the wrecked ship!";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        HttpSession session = req.getSession(false);

        //no session at all - redirect to landing page
        if (session == null) {
            sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req, USER_NOT_LOGGED_IN_NO_SESSION_URL_PARAM);

            return;
        }

        //no username in session - redirect to landing page
        User user = AppHelper.getLoggedInUser(session);
        if (user == null) {
            sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req, USER_NOT_LOGGED_IN_SESSION_URL_PARAM);

            return;
        }

        //user logged in - redirect to homepage
        sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), resp, req);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        //is user filled
        String username = req.getParameter(USERNAME_FORM_PARAM);
        if (username == null || username.equals("")) {
            sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req, NO_USERNAME_PROVIDED_URL_PARAM);

            return;
        }

        //is pass filled
        String password = req.getParameter(PASSWORD_FORM_PARAM);
        if (password == null) {
            sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req, NO_PASS_PROVIDED_URL_PARAM);

            return;
        }

        //removing some mistakes in input - due to autofill in browser, like caps
        //TODO proper practice or not?
        username = username.trim().toLowerCase();

        //does user exist
        if (!UserDAO.exists(username)) {
            sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req, USER_DOES_NOT_EXIST_URL_PARAM);

            return;
        }

        String passForUsername = UserDAO.getPasswordForUsername(username);

        //pass for user null? bad data!
        if (passForUsername == null) {
            sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req);

            return; //TODO jak obslugiwac bad data? nie obslugiwac? TODO test this case
        }

        //is pass correct
        if (passForUsername.equals(password)) {
            HttpSession session = req.getSession();
            session.setAttribute(USERNAME_FORM_PARAM, username);

            sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), 200, resp, req);
        } else { //bad pass
            sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req, BAD_PASS_URL_PARAM);
        }
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }

}
