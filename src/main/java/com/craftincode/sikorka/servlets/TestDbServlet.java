package com.craftincode.sikorka.servlets;

import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.util.Log;
import com.craftincode.sikorka.util.TextHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/" + TestDbServlet.MY_PATH)
public class TestDbServlet extends BaseServlet {
    public static final String MY_PATH = "testDB";

    static {
        Log.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        List<User> users = UserDAO.getAllUsers();

        write(TextHelper.toJsonString(users));
    }

}
