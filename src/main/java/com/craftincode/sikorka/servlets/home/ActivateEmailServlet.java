package com.craftincode.sikorka.servlets.home;

import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/" + ActivateEmailServlet.MY_PATH)
public class ActivateEmailServlet extends BaseServlet {
    public static final String MY_PATH = "home/activate";

    public static final String ACTIVATION_CODE_PARAM_IN_URL = "activationCode";
    public static final String EMAIL_PARAM_IN_URL = "email";

    public static final String ACCOUNT_ACTIVATED_URL_PARAM = "accountActivated";
    public static final String ACCOUNT_ACTIVATED_MSG = "Account activated! YAY!";
    public static final String WRONG_EMAIL_URL_PARAM = "wrongEmail";
    public static final String WRONG_EMAIL_MSG = "Bad luck. Wrong email!";
    public static final String WRONG_ACTIVATION_CODE_URL_PARAM = "wrongActivationCode";
    public static final String WRONG_ACTIVATION_CODE_MSG = "Wrong activation code!";
    public static final String MISSING_EMAIL_URL_PARAM = "missingEmailParam";
    public static final String MISSING_EMAIL_MSG = "Missing email! Can't activate account.";
    public static final String MISSING_ACTIVATION_CODE_URL_PARAM = "missingActivationCode";
    public static final String MISSING_ACTIVATION_CODE_MSG = "Missing activation code!";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        String activationCodeFromUrl = req.getParameter(ACTIVATION_CODE_PARAM_IN_URL);
        System.out.println(activationCodeFromUrl);

        if (activationCodeFromUrl == null || "".equals(activationCodeFromUrl)) {
            write(MISSING_ACTIVATION_CODE_MSG);

            sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), 400, resp, req, MISSING_ACTIVATION_CODE_URL_PARAM);
            return;
        }

        Object userByActivationCode = UserDAO.findUserByActivationCode(activationCodeFromUrl);
        if (userByActivationCode == null) {
            write(WRONG_ACTIVATION_CODE_MSG);

            sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), 400, resp, req, WRONG_ACTIVATION_CODE_URL_PARAM);
            return;
        }

        //TODO if I have HTML escaping in URL this does not work, can't get email param
        //URL changed to for example: "activate?email\u003dbb@cc.pl\u0026activationCode\u003d0fbecec7-efe6-4ae8-b027-9a4e4b214afe"
        String emailFromUrl = req.getParameter(EMAIL_PARAM_IN_URL);
        if (emailFromUrl == null || "".equals(emailFromUrl)) {
            write(MISSING_EMAIL_MSG);

            sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), 400, resp, req, MISSING_EMAIL_URL_PARAM);
            return;
        }

        User user = (User) userByActivationCode;
        if (emailFromUrl.equals(user.getEmail())) {
            //activate account! YAY!
            UserDAO.activate(user.getUsername());

            write(ACCOUNT_ACTIVATED_MSG);

            sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), 200, resp, req, ACCOUNT_ACTIVATED_URL_PARAM);
        } else {
            write(WRONG_EMAIL_MSG);

            sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), 400, resp, req, WRONG_EMAIL_URL_PARAM);
        }
    }

}
