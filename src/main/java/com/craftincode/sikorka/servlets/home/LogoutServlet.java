package com.craftincode.sikorka.servlets.home;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.servlets.BaseServlet;
import com.craftincode.sikorka.servlets.LoginServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/" + LogoutServlet.MY_PATH)
public class LogoutServlet extends BaseServlet {
    public static final String MY_PATH = "home/logout";

    public static final String USER_LOGGED_OUT_URL_PARAM = "userLoggedOut";
    public static final String USER_LOGGED_OUT_MSG = "You are logged out!";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        req.getSession().invalidate();

        sendRedirectWithNOContextPath(LoginServlet.getMyJSP(), 200, resp, req, USER_LOGGED_OUT_URL_PARAM);
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

}
