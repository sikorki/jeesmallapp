package com.craftincode.sikorka.servlets.home.rent;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.CarDAO;
import com.craftincode.sikorka.dao.RentalDAO;
import com.craftincode.sikorka.model.Car;
import com.craftincode.sikorka.model.Rental;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.BaseServlet;
import com.craftincode.sikorka.servlets.LoginServlet;
import com.craftincode.sikorka.util.DateHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.craftincode.sikorka.util.Log.*;

@WebServlet("/" + CalculatePriceServlet.MY_PATH)
public class CalculatePriceServlet extends BaseServlet {
    public static final String MY_PATH = "home/rent/calculatePrice";
    public static final String MY_JSP = "showPriceAndRent.jsp";

    public static final int MAX_DAYS_TO_RENT = 30;

    public static final String EXCEPTION_MSG_ATTRIBUTE_NAME = "exceptionMsg";

    public static final String NUMBER_OF_DAYS_FORM_PARAM_NAME = "number_of_days";
    public static final String FROM_DATE_FORM_PARAM_NAME = "from_date";
    public static final String CAR_ID_FORM_PARAM_NAME = "car_id";

    public static final String NOT_A_NUMBER_OF_DAYS_MSG = "Number of days has to be a positive number not exceeding " + MAX_DAYS_TO_RENT + " days.";
    public static final String NUMBER_OF_DAYS_WRONG_MSG = "Number of days has to be between 1 and " + MAX_DAYS_TO_RENT + ".";
    public static final String NOT_A_DATE_MSG = "Date needs to be in format " + DateHelper.pattern + ".";
    public static final String NOT_A_CAR_ID_MSG = "Car ID is wrong!";
    public static final String DID_NOT_FIND_CAR = "Did not find car available to rent with given ID!";
    public static final String USER_NOT_LOGGED_IN = "User not logged in!";
    private static final String CAR_NOT_AVAILABLE_THOSE_DATES = "Sorry, car is not available those dates. ";
    private static final Object DATE_CANNOT_BE_IN_PAST = "Date cannot be in past!";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        User user = AppHelper.getLoggedInUser(req.getSession());
        if (user == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, USER_NOT_LOGGED_IN);
            forward("/../../" + LoginServlet.MY_JSP, req, resp); //TODO

            return;
        }

        String fromDateString = req.getParameter(FROM_DATE_FORM_PARAM_NAME);
        say(FROM_DATE_FORM_PARAM_NAME + ": " + fromDateString);
        java.sql.Date fromDate = DateHelper.stringToDate(fromDateString);
        if (fromDate == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, NOT_A_DATE_MSG);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        if (DateHelper.isBeforeDay(fromDate, DateHelper.nowDate())) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, DATE_CANNOT_BE_IN_PAST);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        String numberOfDaysString = req.getParameter(NUMBER_OF_DAYS_FORM_PARAM_NAME);
        say(NUMBER_OF_DAYS_FORM_PARAM_NAME + ": " + numberOfDaysString);
        Integer numberOfDays = null;
        try {
            numberOfDays = Integer.parseInt(numberOfDaysString);
        } catch (NumberFormatException e) { //not a number
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, NOT_A_NUMBER_OF_DAYS_MSG);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        if (numberOfDays <= 0 || numberOfDays > MAX_DAYS_TO_RENT) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, NUMBER_OF_DAYS_WRONG_MSG);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        String carIdString = req.getParameter(CAR_ID_FORM_PARAM_NAME);
        say(CAR_ID_FORM_PARAM_NAME + ": " + carIdString);
        Integer carId = null;
        try {
            carId = Integer.parseInt(carIdString);
        } catch (NumberFormatException e) { //not a number
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, NOT_A_CAR_ID_MSG);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        Car carToRent = CarDAO.getCarById(carId);
        if (carToRent == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, DID_NOT_FIND_CAR);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        if (!RentalDAO.isCarAvailableForRent(carId, fromDate, numberOfDays)) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, CAR_NOT_AVAILABLE_THOSE_DATES);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        double toPay = carToRent.getPricePerDay() * numberOfDays;
        Rental possibleRental = RentalDAO.prepareNewRental(user.getUsername(), carToRent.getId(), fromDate, numberOfDays, toPay, carToRent.getPricePerDay());

        req.setAttribute("car", carToRent);
        req.setAttribute("rental", possibleRental);
        forward(MY_JSP, req, resp);
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

}
