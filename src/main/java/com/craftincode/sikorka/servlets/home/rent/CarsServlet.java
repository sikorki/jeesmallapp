package com.craftincode.sikorka.servlets.home.rent;

import com.craftincode.sikorka.AppHelper;

public class CarsServlet {
    public static final String MY_JSP = "home/rent/cars.jsp";

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }

}
