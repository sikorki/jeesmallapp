package com.craftincode.sikorka.servlets.home;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.BaseServlet;
import com.craftincode.sikorka.servlets.LoginServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/" + DeleteAccountByUserServlet.MY_PATH)
public class DeleteAccountByUserServlet extends BaseServlet {
    public static final String MY_PATH = "home/deleteAccountByUser";
    public static final String MY_JSP = "home/deleteAccountByUser.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        resp.sendRedirect(MY_JSP);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        Object username = req.getSession().getAttribute(LoginServlet.USERNAME_FORM_PARAM);

        //user not logged in
        if (username == null || "".equals(username)) {
            sendRedirectWithNOContextPath(LoginServlet.MY_JSP, resp, req);

            return;
        }

        User loggedInUser = UserDAO.findUserByLoginInDB((String) username);
        if (loggedInUser == null) {
            write("No user found to delete!"); //TODO do it with param
            redirectWithDelay(HomePageServlet.MY_JSP, req);

            return;
        }

        if (UserDAO.deleteAccountInDBbyUsername(loggedInUser)) {
            req.getSession().removeAttribute(LoginServlet.USERNAME_FORM_PARAM);

            write("User deleted!"); //TODO do it with param
            redirectWithDelay(LoginServlet.MY_JSP, req);
        }
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }

}
