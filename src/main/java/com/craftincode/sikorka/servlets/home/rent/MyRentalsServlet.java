package com.craftincode.sikorka.servlets.home.rent;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.servlets.BaseServlet;

import javax.servlet.annotation.WebServlet;

@WebServlet("/" + MyRentalsServlet.MY_PATH)
public class MyRentalsServlet extends BaseServlet {
    public static final String MY_PATH = "home/rent/myRentals";
    public static final String MY_JSP = "home/rent/myRentals.jsp";

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }
}
