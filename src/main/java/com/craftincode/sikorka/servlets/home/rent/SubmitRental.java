package com.craftincode.sikorka.servlets.home.rent;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.CarDAO;
import com.craftincode.sikorka.dao.RentalDAO;
import com.craftincode.sikorka.model.Car;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.BaseServlet;
import com.craftincode.sikorka.servlets.LoginServlet;
import com.craftincode.sikorka.util.DateHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static com.craftincode.sikorka.util.Log.*;
import static com.craftincode.sikorka.servlets.home.rent.CalculatePriceServlet.*;

@WebServlet("/" + SubmitRental.MY_PATH)
public class SubmitRental extends BaseServlet {
    public static final String MY_PATH = "home/rent/submitRental";

    private static final String SUCCESS_MSG_ATTRIBUTE_NAME = "Car rented!";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        User user = AppHelper.getLoggedInUser(req.getSession());
        if (user == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, USER_NOT_LOGGED_IN);
            forward("/" + LoginServlet.MY_JSP, req, resp);

            return;
        }

        String numberOfDaysString = req.getParameter(NUMBER_OF_DAYS_FORM_PARAM_NAME);
        say(NUMBER_OF_DAYS_FORM_PARAM_NAME + ": " + numberOfDaysString);
        Integer numberOfDays = null;
        try {
            numberOfDays = Integer.parseInt(numberOfDaysString);
        } catch (NumberFormatException e) { //not a number
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, NOT_A_NUMBER_OF_DAYS_MSG);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        if (numberOfDays <= 0 || numberOfDays > MAX_DAYS_TO_RENT) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, NUMBER_OF_DAYS_WRONG_MSG);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        String fromDateString = req.getParameter(FROM_DATE_FORM_PARAM_NAME);
        say(FROM_DATE_FORM_PARAM_NAME + ": " + fromDateString);
        Date fromDate = DateHelper.stringToDate(fromDateString);
        if (fromDate == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, NOT_A_DATE_MSG);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        String carIdString = req.getParameter(CAR_ID_FORM_PARAM_NAME);
        say(CAR_ID_FORM_PARAM_NAME + ": " + carIdString);
        Integer carId = null;
        try {
            carId = Integer.parseInt(carIdString);
        } catch (NumberFormatException e) { //not a number
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, NOT_A_CAR_ID_MSG);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        Car carToRent = CarDAO.getCarById(carId);
        if (carToRent == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, DID_NOT_FIND_CAR);
            forward("/" + PrepareToRentServlet.MY_JSP, req, resp);

            return;
        }

        double toPay = carToRent.getPricePerDay() * numberOfDays;

        if (RentalDAO.rentAcar(user.getUsername(), carToRent.getId(), fromDate, numberOfDays, toPay, carToRent.getPricePerDay()) > 0) {
            write("car rented");
            req.setAttribute(SUCCESS_MSG_ATTRIBUTE_NAME, "Car rented!");
            forward("/" + CarsServlet.MY_JSP, req, resp);
        } else {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, "Car not rented.");
            forward("/" + CarsServlet.MY_JSP, req, resp);
        }
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

}
