package com.craftincode.sikorka.servlets.home.rent;

import com.craftincode.sikorka.AppHelper;

public class PrepareToRentServlet {
    public static final String MY_JSP = "home/rent/prepareToRent.jsp";

    public static final String CAR_ID_PARAM_NAME = "car_id";

    public static final String CAR_ID_NOT_CORRECT_URL_PARAM = "badCarID";
    public static final String CAR_ID_NOT_CORRECT_MSG = "Car ID ain't correct!";

    public static final String CAR_NOT_FOUND_URL_PARAM = "carNotFound";
    public static final String CAR_NOT_FOUND_MSG = "Car not found.";

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }
}
