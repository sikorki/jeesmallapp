package com.craftincode.sikorka.servlets.admin.rent;

import com.craftincode.sikorka.AppHelper;

public class AdminRentalsServlet {
    public static final String MY_JSP = "admin/rent/adminRentals.jsp";

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }
}
