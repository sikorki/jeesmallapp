package com.craftincode.sikorka.servlets.admin;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.BaseServlet;
import com.craftincode.sikorka.servlets.LoginServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/" + DeleteAccountByAdminServlet.MY_PATH)
public class DeleteAccountByAdminServlet extends BaseServlet {
    public static final String MY_PATH = "admin/deleteAccountByAdmin";
    public static final String MY_JSP = "admin/deleteAccountByAdmin.jsp";

    public static final String USERNAME_TO_BE_DELETED_URL_PARAM = "usernameToBeDeleted";

    public static final String WONT_DELETE_USER_URL_PARAM = "wontDeleteCurrentUser";
    public static final String WONT_DELETE_USER_MSG = "Won't delete current user!";
    public static final String USER_SUCCCESSFULLY_DELETED_URL_PARAM = "userDeleted";
    public static final String USER_SUCCCESSFULLY_DELETED_MSG = "User successfully deleted!";
    public static final String NO_USER_FOUND_TO_DELETE_URL_PARAM = "noUserFoundToDelete";
    public static final String NO_USER_FOUND_TO_DELETE_MSG = "No user found to delete!";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        String username = req.getParameter(DeleteAccountByAdminServlet.USERNAME_TO_BE_DELETED_URL_PARAM);

        User userToDelete = UserDAO.findUserByLoginInDB(username);
        if (userToDelete == null) {
            sendRedirectWithNOContextPath(AdminUsersServlet.getMyJSP(), 400, resp, req, NO_USER_FOUND_TO_DELETE_URL_PARAM);

            return;
        }

        User currentUser = UserDAO.findUserByLoginInDB((String) req.getSession().getAttribute(LoginServlet.USERNAME_FORM_PARAM));
        if (userToDelete.equals(currentUser)) {
            sendRedirectWithNOContextPath(AdminUsersServlet.getMyJSP(), 400, resp, req, WONT_DELETE_USER_URL_PARAM);

            return;
        }

        if (UserDAO.deleteAccountInDBbyUsername(userToDelete)) {
            sendRedirectWithNOContextPath(AdminUsersServlet.getMyJSP(), 200, resp, req, USER_SUCCCESSFULLY_DELETED_URL_PARAM);
        }
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }

}
