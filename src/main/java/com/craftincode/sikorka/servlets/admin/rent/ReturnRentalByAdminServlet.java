package com.craftincode.sikorka.servlets.admin.rent;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.RentalDAO;
import com.craftincode.sikorka.model.Rental;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.BaseServlet;
import com.craftincode.sikorka.servlets.LoginServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/" + ReturnRentalByAdminServlet.MY_PATH)
public class ReturnRentalByAdminServlet extends BaseServlet {
    public static final String MY_PATH = "admin/rent/returnRentalByAdmin";
    public static final String MY_JSP = "adminRentals.jsp";

    public static final String EXCEPTION_MSG_ATTRIBUTE_NAME = "exceptionMsg";
    public static final String USER_NOT_LOGGED_IN = "User not logged in!";
    public static final String RENTAL_ID_IS_NULL = "Oops! Rental ID is null.";
    public static final String RENTAL_ID_IS_NOT_A_NUMBER = "Rental ID needs to be a number.";
    public static final String RENTAL_DOES_NOT_EXIST = "Rental does not exist!";
    public static final Object RENTAL_NOT_RETURNED = "Rental not returned :/";

    public static final String SUCCESS_MSG_ATTRIBUTE_NAME = "successMsg";
    public static final Object RENTAL_RETURNED = "Rental returned!";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        sendRedirectWithNOContextPath(getMyJSP(), 400, resp, req);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        User user = AppHelper.getLoggedInUser(req.getSession());
        if (user == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, USER_NOT_LOGGED_IN);
            forward("/../../" + LoginServlet.MY_JSP, req, resp); //TODO

            return;
        }

        String rentalIdString = req.getParameter("rentalId");
        if (rentalIdString == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, RENTAL_ID_IS_NULL);
            forward(MY_JSP, req, resp);

            return;
        }

        Integer rentalId = null;
        try {
            rentalId = Integer.valueOf(rentalIdString);
        } catch (NumberFormatException e) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, RENTAL_ID_IS_NOT_A_NUMBER);
            forward(MY_JSP, req, resp);

            return;
        }

        Rental rental = RentalDAO.getRental(rentalId);
        if (rental == null) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, RENTAL_DOES_NOT_EXIST);
            forward(MY_JSP, req, resp);

            return;
        }

        if (RentalDAO.returnRental(rental) > 0) {
            req.setAttribute(SUCCESS_MSG_ATTRIBUTE_NAME, RENTAL_RETURNED);
            forward(MY_JSP, req, resp);

            return;
        } else {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE_NAME, RENTAL_NOT_RETURNED);
            forward(MY_JSP, req, resp);

            return;
        }
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }
}
