package com.craftincode.sikorka.servlets.admin;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/" + EditAccountByAdminServlet.MY_PATH)
public class EditAccountByAdminServlet extends BaseServlet {
    public static final String MY_JSP = "admin/editAccountByAdmin.jsp";
    public static final String MY_PATH = "admin/editAccountByAdmin";

    public static final String USERNAME_TO_BE_EDITED_FORM_PARAM = "usernameToBeEdited";
    public static final String EMAIL_TO_BE_EDITED_FORM_PARAM = "emailToBeEdited";
    public static final String PASSWORD_TO_BE_EDITED_FORM_PARAM = "passwordToBeEdited";

    public static final String USER_SUCCCESSFULLY_EDITED_URL_PARAM = "userEdited";
    public static final String USER_SUCCCESSFULLY_EDITED_MSG = "User successfully edited!";
    public static final String NO_USER_FOUND_TO_EDIT_URL_PARAM = "noUserFoundToEdit";
    public static final String NO_USER_FOUND_TO_EDIT_MSG = "No user found to edit!";
    public static final String USER_COULD_NOT_BE_SAVED_IN_DB_URL_PARAM = "userNotSaved";
    public static final String USER_COULD_NOT_BE_SAVED_IN_DB_MSG = "User could not be saved!";

    public static final String PASS_CANT_BE_NULL_URL_PARAM = "passCantBeNull";
    public static final String PASS_CANT_BE_NULL_MSG = "Password can't be null!";
    public static final String EMAIL_CANT_BE_NULL_OR_EMPTY_URL_PARAM = "emailCantBeNull";
    public static final String EMAIL_CANT_BE_NULL_MSG = "Email can't be null, neither empty!";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        System.out.println("Redirect URL: " + AdminUsersServlet.getMyJSP());
        resp.sendRedirect(AdminUsersServlet.getMyJSP());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        String username = req.getParameter(USERNAME_TO_BE_EDITED_FORM_PARAM);

        User userToEdit = UserDAO.findUserByLoginInDB(username);
        if (userToEdit == null) {
//            write("No user found to delete!"); //TODO do it with param
//            redirectWithDelay(AdminServlet.MY_JSP, req);

            sendRedirectWithNOContextPath(AdminUsersServlet.getMyJSP() + "?" + NO_USER_FOUND_TO_EDIT_URL_PARAM, 400, resp, req);

            return;
        }

        String newPassword = req.getParameter(PASSWORD_TO_BE_EDITED_FORM_PARAM);

        if (newPassword == null) {
            sendRedirectWithNOContextPath(AdminUsersServlet.getMyJSP() + "?" + PASS_CANT_BE_NULL_URL_PARAM, 400, resp, req);

            return;
        }

        String newEmail = req.getParameter(EMAIL_TO_BE_EDITED_FORM_PARAM);
        if (newEmail == null || "".equals(newEmail)) {
            sendRedirectWithNOContextPath(AdminUsersServlet.getMyJSP() + "?" + EMAIL_CANT_BE_NULL_OR_EMPTY_URL_PARAM, 400, resp, req);

            return;
        }

        userToEdit.setPassword(newPassword);
        userToEdit.setEmailAndDeactivate(newEmail);

        if (UserDAO.updateUserInDBbyUsername(userToEdit))
            sendRedirectWithNOContextPath(AdminUsersServlet.getMyJSP() + "?" + USER_SUCCCESSFULLY_EDITED_URL_PARAM, 200, resp, req);
        else
            sendRedirectWithNOContextPath(AdminUsersServlet.getMyJSP() + "?" + USER_COULD_NOT_BE_SAVED_IN_DB_URL_PARAM, 400, resp, req);
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }

}
