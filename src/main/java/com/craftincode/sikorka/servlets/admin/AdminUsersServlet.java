package com.craftincode.sikorka.servlets.admin;

import com.craftincode.sikorka.AppHelper;

public class AdminUsersServlet {
    public static final String MY_JSP = "admin/users.jsp";

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }
}
