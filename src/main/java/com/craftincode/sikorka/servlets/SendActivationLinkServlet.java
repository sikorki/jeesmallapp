package com.craftincode.sikorka.servlets;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.home.HomePageServlet;
import com.craftincode.sikorka.util.EmailHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;

@WebServlet("/" + SendActivationLinkServlet.MY_PATH)
public class SendActivationLinkServlet extends BaseServlet {
    public static final String MY_PATH = "home/sendActivationLink";

    public static final String EMAIL_TEMPLATE_TEXT_TO_REPLACE = "ACTIVATION_LINK";

    public static final String ACTIVATION_EMAIL_SENT_MSG = "Email sent!";
    public static final String ACTIVATION_EMAIL_SENT_URL_PARAM = "activationEmailSent";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), resp, req);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        Object username = req.getSession().getAttribute(LoginServlet.USERNAME_FORM_PARAM);
        if (username == null) {
            //TODO support error situations
            write("There is no user in session. Noone to activate! ");

            return;
        }

        User user = UserDAO.findUserByLoginInDB((String) username);
        if (user == null) {
            //TODO support error situations
            write("No user found by username. Noone to activate! ");

            return;
        }

        String emailAddress = req.getParameter("email");
        if (emailAddress == null || "".equals(emailAddress)) {
            //TODO support error situations
            write("No email provided. Can't activate account without email. ");

            return;
        }

        //TODO email address verification
        user.setEmailAndDeactivate(emailAddress);
        if (!UserDAO.updateUserInDBbyUsername(user)) {
            //TODO support error situations
            write("User not updated in DB!");

            return;
        }

        String activationLink = null;
        try {
            activationLink = AppHelper.getAppUrl() + user.getActivationPathForActivationLink();
        } catch (MalformedURLException e) {
            e.printStackTrace();

            //TODO support error situations
            write("Could not send activation link! Wrong App URL ☹️");

            return;
        }

        String content = AppHelper.getFileContentFromResources("email.html");
        content = content.replaceFirst(EMAIL_TEMPLATE_TEXT_TO_REPLACE, activationLink);

        EmailHelper.sendEmail(emailAddress,
                content,
                "Activation email from " + AppHelper.APP_NAME_IN_URL);

        write(ACTIVATION_EMAIL_SENT_MSG);

        sendRedirectWithNOContextPath(HomePageServlet.getMyJSP(), 200, resp, req, ACTIVATION_EMAIL_SENT_URL_PARAM);
    }

}
