package com.craftincode.sikorka.servlets;

import com.craftincode.sikorka.util.HttpHelper;
import com.craftincode.sikorka.util.Log;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public abstract class BaseServlet extends HttpServlet {
    private PrintWriter writer;

    private void setDefaultResposneProperties(HttpServletResponse resp) {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
    }

    private void setPrintWriter(HttpServletResponse resp) {
        try {
            writer = resp.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String s) {
        writer.write(s + "<br/>\n");
    }

    public void writeln(String s) {
        write(s);
        write("");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setDefaultResposneProperties(resp);
        setPrintWriter(resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setDefaultResposneProperties(resp);
        setPrintWriter(resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setDefaultResposneProperties(resp);
        setPrintWriter(resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setDefaultResposneProperties(resp);
        setPrintWriter(resp);
    }


    protected void redirectWithDelay(HttpServletRequest req) {
        write("<meta http-equiv=\"Refresh\" content=\"2;url=" + req.getHeader("referer") + "\">");
    }

    protected void redirectWithDelay(String url, HttpServletRequest req) {
        write("<meta http-equiv=\"Refresh\" content=\"2;url=" + url + "\">");
    }

    public void sendRedirectWithNOContextPath(String url, int status, HttpServletResponse resp, HttpServletRequest req) throws IOException {
        HttpHelper.sendRedirectNOcontextPath(url, status, resp, req);
    }

    public void sendRedirectWithNOContextPath(String url, int status, HttpServletResponse resp, HttpServletRequest req, String... parameters) throws IOException {
        HttpHelper.sendRedirectNOcontextPath(url, status, resp, req, parameters);
    }

    public void sendRedirectWithNOContextPath(String url, HttpServletResponse resp, HttpServletRequest req, String... parameters) throws IOException {
        HttpHelper.sendRedirectNOcontextPath(url, resp.getStatus(), resp, req, parameters);
    }

    public void sendRedirectWithNOContextPath(String url, HttpServletResponse resp, HttpServletRequest req) throws IOException {
        HttpHelper.sendRedirectNOcontextPath(url, resp.getStatus(), resp, req);
    }

    public void forward(String s, HttpServletRequest req, HttpServletResponse resp) {
        RequestDispatcher rd = req.getRequestDispatcher(s);

        try {
            rd.forward(req, resp);

            Log.say("Forwarded " + rd.toString());
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
