package com.craftincode.sikorka.servlets;

import com.craftincode.sikorka.AppHelper;
import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/" + RegisterServlet.MY_PATH)
public class RegisterServlet extends BaseServlet {
    public static final String MY_PATH = "register";
    public static final String MY_JSP = "register.jsp";

    public static final String USER_ADDED_URL_PARAM = "userAdded";
    public static final String USER_ADDED_MSG = "User added!";

    public static final String ERROR_OCCURED_URL_PARAM = "sthWentWrong";
    public static final String ERROR_OCCURED_MSG = "Something went wrong! ";
    public static final String EXCEPTION_MSG_ATTRIBUTE = "exceptionMsg";


    @Override
    //only opens the page
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

        sendRedirectWithNOContextPath(getMyJSP(), resp, req);
    }

    @Override
    //someone is registering on the register JSP page
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);

        String username = req.getParameter(LoginServlet.USERNAME_FORM_PARAM);
        String password = req.getParameter(LoginServlet.PASSWORD_FORM_PARAM);

        try {
            UserDAO.addNewUser(username, password);

            sendRedirectWithNOContextPath(LoginServlet.getMyJSP(), 200, resp, req, USER_ADDED_URL_PARAM);
        } catch (Exception e) {
            req.setAttribute(EXCEPTION_MSG_ATTRIBUTE, e.getMessage());

            RequestDispatcher rd = req.getRequestDispatcher(MY_JSP);
            rd.forward(req, resp);
        }
    }

    public static String getMyPath() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_PATH;
    }

    public static String getMyJSP() {
        return "/" + AppHelper.APP_NAME_IN_URL + "/" + MY_JSP;
    }

}
