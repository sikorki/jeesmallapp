package com.craftincode.sikorka.model;

import com.craftincode.sikorka.util.TextHelper;
import com.craftincode.sikorka.util.Util;

import java.awt.*;
import java.util.Objects;

public class Car {
    private Integer id;
    private Color color;
    private int seats;
    private String brand;
    private int productionYear;
    private double horsePower;
    private double pricePerDay;

    public Car(Integer id, String color, Integer seats, String brand,
               Integer prodYear, Integer horsePower, Double pricePerDay) {
        this.id = id;
        this.color = Util.hexToColor(color);
        this.seats = seats;
        this.brand = brand;
        this.productionYear = prodYear;
        this.horsePower = horsePower;
        this.pricePerDay = pricePerDay;
    }

    public Car(Integer id, Color color, Integer seats, Brand brand,
               Integer prodYear, Integer horsePower, Double pricePerDay) {
        this.id = id;
        this.color = color;
        this.seats = seats;
        this.brand = String.valueOf(brand);
        this.productionYear = prodYear;
        this.horsePower = horsePower;
        this.pricePerDay = pricePerDay;
    }

    public String colorInHex() {
        return Util.colorToHex(this.color);
    }

    public enum Brand { //TODO make it a static list
        VOLVO,
        MATIZ,
        BMW,
        CADILAC,
        SEAT,
        SAAB
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setBrand(Brand brand) {
        this.brand = String.valueOf(brand);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getBrand() {
        return brand;
    }

    public Color getColor() {
        return color;
    }

    public int getSeats() {
        return seats;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public double getHorsePower() {
        return horsePower;
    }

    public double getPricePerDay() {
        return pricePerDay;
    }

    public void setHorsePower(double horsePower) {
        this.horsePower = horsePower;
    }

    public void setPricePerDay(double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    @Override
    public String toString() {
        return toJson();
    }

    public String toJson() {
        return TextHelper.toJsonString(this);
    }

    public static Car fromJson(String jsonString) {
        return TextHelper.gson.fromJson(jsonString, Car.class);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj instanceof Car) {
            Car car = (Car) obj;

            if (Objects.equals(color, car.color)
                    && Objects.equals(seats, car.seats)
                    && TextHelper.equalsIgnoreCase(brand, car.brand)
                    && Objects.equals(productionYear, car.productionYear)
                    && Objects.equals(horsePower, car.horsePower)
                    && Objects.equals(pricePerDay, car.pricePerDay)
                )
                return true;
        }

        return false;
    }

    public Integer getId() {
        return id;
    }

    public static void main(String[] args) {
        System.out.println(String.valueOf(Brand.VOLVO));
    }

}
