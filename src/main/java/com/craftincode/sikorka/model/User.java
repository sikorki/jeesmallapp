package com.craftincode.sikorka.model;

import com.craftincode.sikorka.servlets.home.ActivateEmailServlet;
import com.craftincode.sikorka.util.TextHelper;

import static com.craftincode.sikorka.util.TextHelper.*;

import java.util.Objects;
import java.util.UUID;

public class User {
    private String username;
    private String password; //TODO ??? jak przechowywac hasla zaszyfrowane?
    private String email;
    private Role role = Role.USER; //by default a user has USER role (is not an ADMIN)
    private String activationCode;
    private String activationPath;
    private boolean activated = false;

    /** When I set or change email I need to activate account. */
    public void setEmailAndDeactivate(String email) {
        setEmail(email);
        generateNewActivationCodeAndDeactivate();
    }

    //TODO date of activation
    //TODO invalidate activation within 20 minutes from activation request

    public enum Role {
        USER,
        ADMIN
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, String role, String email, String actCode, String actPath, boolean activated) {
        this.username = username;
        this.password = password;
        this.role = (role == null ? Role.USER : Role.valueOf(role));
        this.email = email;
        this.activated = activated;
        this.activationCode = actCode;
        this.activationPath = actPath;
    }

    public boolean isActivated() {
        return activated;
    }

    public User setAccountActivated() {
        activated = true;

        return this;
    }

    private void setNotActivated() {
        activated = false;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String newPassword) {
        password = newPassword;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /** When I generate new activation code I set the account to not active. */
    public void generateNewActivationCodeAndDeactivate() {
        activationCode = UUID.randomUUID().toString();

        setActivationPathForActivationLink();

        setNotActivated();
    }

    public String getEmail() {
        return email;
    }

    public String getActivationCode() {
        return activationCode;
    }

    private void setActivationPathForActivationLink() {
        activationPath =
                "" + ActivateEmailServlet.MY_PATH +
                "?" + ActivateEmailServlet.EMAIL_PARAM_IN_URL + "=" + email +
                "&" + ActivateEmailServlet.ACTIVATION_CODE_PARAM_IN_URL + "=" + activationCode; //TODO do it with URL class
    }

    public String getActivationPathForActivationLink() {
        return activationPath;
    }

    @Override
    public String toString() {
        return toJson();
    }

    public String toJson() {
        return toJsonString(this);
    }

    public static User fromJson(String jsonString) {
        return TextHelper.gson.fromJson(jsonString, User.class);
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj instanceof User) {
            User user = (User) obj;

            if ((equalsIgnoreCase(username, user.username)) &&
                    Objects.equals(password, user.password) &&
                    equalsIgnoreCase(email, user.email) &&
                    Objects.equals(activated, user.activated) &&
                    Objects.equals(activationCode, user.activationCode) &&
                    Objects.equals(activationPath, user.activationPath))
                return true;
        }

        return false;
    }

    public Role getRole() {
        if (role == null)
            return Role.USER;

        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isAdmin() {
        return Role.ADMIN.equals(role);
    }

    public void setAdmin() {
        role = Role.ADMIN;
    }
}
