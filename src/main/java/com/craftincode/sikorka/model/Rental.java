package com.craftincode.sikorka.model;

import static com.craftincode.sikorka.util.DateHelper.*;
import static com.craftincode.sikorka.util.Log.*;

import com.craftincode.sikorka.util.TextHelper;

import java.sql.Date;
import java.util.Objects;
import java.util.Random;

public class Rental {
    private Integer id;
    private String username;
    private int carId;
    private Date dateStarted;
    private Date dateReturned;
    private int daysToRent;
    private String comment;
    private double pricePaid;
    private double priceDue;
    private double pricePerDay;

    public Rental(Integer id, String username, int carId, Date dateStarted, Date dateReturned, int daysToRent, String comment,
                  double pricePaid, double priceDue, double pricePerDay) {
        this.id = id;
        if (id == null) {
            this.id = new Random().nextInt();
        }
        this.username = username;
        this.carId = carId;
        this.dateStarted = dateStarted;
        this.dateReturned = dateReturned;
        this.daysToRent = daysToRent;
        this.comment = comment;
        this.pricePaid = pricePaid;
        this.priceDue = priceDue;
        this.pricePerDay = pricePerDay;
    }

    @Override
    public String toString() {
        String json = TextHelper.toJsonString(this);

        return json +
                " date to return: " + dateToReturn() + ", " +
                "current: " + isRentedNow() + ", " +
                "overdue: " + isOverdue() + ", " +
                "is past: " + isPast() + ", " +
                "is future: " + isFuture();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof Rental))
            return false;

        Rental rental = (Rental) obj;

        return (Objects.equals(rental.carId, carId)
                && TextHelper.equalsIgnoreCase(rental.username, username)
                && Objects.equals(rental.dateStarted, dateStarted)
                && Objects.equals(rental.dateReturned, dateReturned)
                && Objects.equals(rental.daysToRent, daysToRent)
                && TextHelper.equalsIgnoreCase(rental.comment, comment)
        );
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Date getDateReturned() {
        return dateReturned;
    }

    public Date getDateStarted() {
        return dateStarted;
    }

    public double getPriceDue() {
        return priceDue;
    }

    public double getPricePaid() {
        return pricePaid;
    }

    public int getCarId() {
        return carId;
    }

    public int getDaysToRent() {
        return daysToRent;
    }

    public String getComment() {
        return comment;
    }

    public boolean isReturned() {
        return dateReturned != null;
    }

    public Date dateToReturn() {
        if (daysToRent <= 1)
            return dateStarted;

        return datePlusDays(dateStarted, daysToRent - 1);
    }

    public int daysOverdue() {
        if (!isOverdue())
            return 0;

        return daysDifferenceToNow(dateToReturn());
    }

    public boolean isOverdue() {
        Date dateToReturn = dateToReturn();
        say("Date to return: " + dateToReturn);

        if (!isReturned()) {
            if (isBeforeDay(dateToReturn, nowDate())) { // zwrot powinien juz nastapic (przed "teraz")
                say("dateToReturn: " + dateToReturn + " is before nowDate: " + nowDate() + " (for NOT returned rental) so overdue is TRUE");
                return true; //supposed to be returned already
            }
        } else {
            if (isBeforeDay(dateToReturn, dateReturned)) {
                say("dateToReturn: " + dateToReturn + " is before dateReturned: " + dateReturned + " (for returned rental) so overdue is TRUE");
                return true;
            }
        }

        return false;
    }

    /**
     * If currently not available - rented and not returned, and not cancelled, then it is current.
     * Current rental might be overdue.
     * */
    public boolean isRentedNow() {
        if (!isReturned()) {
            java.util.Date now = nowDate();

            boolean nowIsAfterStartDateOfRental = isAfterDay(now, dateStarted);
            boolean nowEqualsStartDateOfRental = isSameDay(now, dateStarted);

            //still rented
            return (nowIsAfterStartDateOfRental || nowEqualsStartDateOfRental);
        }

        return false;
    }

    public boolean isFuture() {
        if (isBeforeDay(nowDate(), dateStarted)) //still rented
            return true;

        return false;
    }

    public boolean isPast() {
        return !isRentedNow() && !isFuture();
    }

    public String toJson() {
        return TextHelper.toJsonString(this);
    }

    public static Rental fromJson(String rentalInJson) {
        if (rentalInJson == null)
            return null;

        return TextHelper.gson.fromJson(rentalInJson, Rental.class);
    }

    public double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public boolean overlaps(java.util.Date fromDateOfNewRental, Date toDateOfNewRental) {
        boolean isToDateOfNewRentalBeforeDateStarted = isBeforeDay(toDateOfNewRental, this.dateStarted);
        say("isToDate of new rental: " + toDateOfNewRental + " before dateStarted of current rental: " + dateStarted + " = " + isToDateOfNewRentalBeforeDateStarted);

        Date dateToReturnPlusDaysOverdue = datePlusDays(dateToReturn(), daysOverdue());
        say("dateToReturn " + dateToReturn() + " + daysOverdue " + daysOverdue() + " = " + dateToReturnPlusDaysOverdue);

        boolean isFromDateOfNewRentalAfterDateToReturnWithPossibleDaysOverdue = isAfterDay(fromDateOfNewRental, dateToReturnPlusDaysOverdue);
        say("isFromDate: " + fromDateOfNewRental + " after dateToReturnWithPossibleDaysOverdue: " + dateToReturnPlusDaysOverdue + " = " + isFromDateOfNewRentalAfterDateToReturnWithPossibleDaysOverdue);

         if (isToDateOfNewRentalBeforeDateStarted
                || isFromDateOfNewRentalAfterDateToReturnWithPossibleDaysOverdue)
            return false;

         return true;
    }

    public void returnMe() {
        dateReturned = nowDate();
    }

}
