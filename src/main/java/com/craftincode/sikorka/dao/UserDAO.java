package com.craftincode.sikorka.dao;

import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.util.DbHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Pattern;

import static com.craftincode.sikorka.util.Log.*;

public class UserDAO {

    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_ACTIVATION_CODE = "activation_code";
    public static final String COLUMN_ACTIVATION_PATH = "activation_path";
    public static final String COLUMN_ACTIVATED = "activated";
    public static final String COLUMN_ROLE = "role";

    public static final String SELECT_ALL_USERS = "select * from carsrental.user;";
    public static final String SELECT_USER__BY_USERNAME = "SELECT * FROM carsrental.user WHERE username = ?;";
    public static final String SELECT_USER__BY_ACTIVATION_CODE = "SELECT * FROM carsrental.user WHERE activation_code = ?;";
    public static final String ADD_USER__WITH_USERNAME_PASSWORD =
            "INSERT INTO carsrental.user (username, password, role, email, activation_code, activation_path, activated) " +
                    "VALUES (?, ?, null, null, null, null, null);";
    public static final String UPDATE_USER__ALL_FIELDS =
            "UPDATE carsrental.user\n" +
                    "SET " +
                    "password = ?, " +
                    "role = ?, " +
                    "email = ?, " +
                    "activation_code = ?, " +
                    "activation_path = ?, " +
                    "activated = ? " +
                    "WHERE username = ?;";
    public static final String DELETE_USER__BY_USERNAME = "DELETE FROM carsrental.user WHERE username = ?;";



    public static final int MAX_USERNAME_LENGTH = 30;
    public static final int MIN_USERNAME_LENGTH = 2;
    private static final String USERNAME_PATTERN = "^([a-zA-Z]+)$";

    public static final int MAX_PASS_LENGTH = 30;
    public static final int MIN_PASS_LENGTH = 0;
    private static final String PASS_PATTERN = "^([a-zA-Z0-9]+)$";

    //user creation exception messages
    public static final String EXC_TOO_LONG_USERNAME = "too long username (exceeds " + MAX_USERNAME_LENGTH + " characters)";
    public static final String EXC_PASS_CANT_BE_NULL = "pass can't be null";
    public static final String EXC_USER_ALREADY_EXISTS = "can't add same user twice";
    public static final String EXC_USERNAME_CANT_BE_EMPTY_STRING = "username can't be empty string";
    public static final String EXC_USERNAME_CANT_BE_NULL = "username can't be null";
    public static final String EXC_TOO_SHORT_USERNAME = "too short username";
    public static final String EXC_USERNAME_CAN_ONLY_HAVE_LETTERS = "username can only have letters";
    public static final String EXC_PASS_CAN_ONLY_HAVE_LETTERS_AND_NUMBERS = "pass can only have letters and numbers";
    public static final String EXC_TOO_LONG_PASS = "too long pass (exceeds " + MAX_PASS_LENGTH + " characters)";

    private static final String EXC_USERNAME_NOT_FOUND = "username not found";

    /** Can add a user:
     * <li>with empty pass, but not with null pass, neither with empty login or null login.
     * <li>2-30 letters only and only chars in username
     * <li>pass can be empty, otherwise up to 30 chars, only letters and numbers
     * */
    public static boolean addNewUser(String login, String pass) throws Exception {
        if (login == null)
            throw new Exception(EXC_USERNAME_CANT_BE_NULL);

        login = login.trim();

        if ("".equals(login))
            throw new Exception(EXC_USERNAME_CANT_BE_EMPTY_STRING);

        if (pass == null)
            throw new Exception(EXC_PASS_CANT_BE_NULL);

        login = login.toLowerCase();

        //can't add same username
        //first checking existence then pattern bc patterns could have been different in past and user could have been added
        if (findUserByLoginInDB(login) != null)
            throw new Exception(EXC_USER_ALREADY_EXISTS);

        //username 2-30 letters only
        if (login.length() < MIN_USERNAME_LENGTH)
            throw new Exception(EXC_TOO_SHORT_USERNAME);

        if (login.length() > MAX_USERNAME_LENGTH)
            throw new Exception(EXC_TOO_LONG_USERNAME);

        //only chars in username
        if (!Pattern.matches(USERNAME_PATTERN, login))
            throw new Exception(EXC_USERNAME_CAN_ONLY_HAVE_LETTERS);

        //pass can be empty
        if ("".equals(pass)) {
            //go on further
        } else {

            //otherwise pass is up to 30 chars,
            if (pass.length() > MAX_PASS_LENGTH)
                throw new Exception(EXC_TOO_LONG_PASS);

            //only letters and numbers
            if (!Pattern.matches(PASS_PATTERN, pass))
                throw new Exception(EXC_PASS_CAN_ONLY_HAVE_LETTERS_AND_NUMBERS);
        }

        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DbHelper.connect();

            ps = DbHelper.prepareStatement(connection, ADD_USER__WITH_USERNAME_PASSWORD);

            ps.setString(1, login);
            ps.setString(2, pass);

            if (DbHelper.executeModify(ps) > 0)
                return true;

        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong with result set :/");
        } finally {
            DbHelper.close(connection);
            DbHelper.close(ps);
            //TODO should I be closing result set in ps?
        }

        return false;
    }

    /** Returns null if user not found or login is null / empty. */
    public static String getPasswordForUsername(String username) {
        if (username == null || "".equals(username))
            return null;

        User userWithUsername = findUserByLoginInDB(username);
        if (userWithUsername == null)
            return null;

        return userWithUsername.getPassword();
    }

    public static boolean exists(String username) {
        if (username == null)
            return false;

        return findUserByLoginInDB(username) != null;
    }

    public static User findUserByLoginInDB(String username) {
        if (username == null)
            return null;

        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, SELECT_USER__BY_USERNAME);
            //TODO make sure in DB usernames are saved in small letters (?)
            ps.setString(1, username.toLowerCase());

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();

                if (rs.first())
                    return createUserFromResultSet(rs);
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong with result set :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }

    public static User findUserByActivationCode(String activationCodeFromUrl) {
        if (activationCodeFromUrl == null)
            return null;

        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, SELECT_USER__BY_ACTIVATION_CODE);
            ps.setString(1, activationCodeFromUrl);

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();

                if (rs.first())
                    return createUserFromResultSet(rs);
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }

    public static List<User> getAllUsers() {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, SELECT_ALL_USERS);

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();

                List<User> users = new ArrayList<User>();

                while (rs.next()) {
                    User user = createUserFromResultSet(rs);
                    users.add(user);
                }

                return users;
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }

    private static User createUserFromResultSet(ResultSet resultSet) {
        try {
            String username = resultSet.getString(COLUMN_USERNAME);
            String password = resultSet.getString(COLUMN_PASSWORD);
            String email = resultSet.getString(COLUMN_EMAIL);
            boolean activated = resultSet.getBoolean(COLUMN_ACTIVATED);
            String actPath = resultSet.getString(COLUMN_ACTIVATION_PATH);
            String actCode = resultSet.getString(COLUMN_ACTIVATION_CODE);
            String role = resultSet.getString(COLUMN_ROLE);

            return new User(username, password, role, email, actCode, actPath, activated);
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong with result set data retrieval.");
        }

        return null;
    }

    public static boolean deleteAccountInDBbyUsername(String username) {
        if (username == null)
            return false;

        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, DELETE_USER__BY_USERNAME);
            ps.setString(1, username);

            if (DbHelper.executeModify(ps) > 0) {
                return true;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong with setting data in prepped statement :/");
        } finally {
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return false;
    }

    public static boolean deleteAccountInDBbyUsername(User user) {
        if (user == null)
            return false;

        return deleteAccountInDBbyUsername(user.getUsername());
    }

    public static boolean updateUserInDBbyUsername(User user) {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, UPDATE_USER__ALL_FIELDS);

            ps.setString(1, user.getPassword());
            ps.setString(2, String.valueOf(user.getRole()));
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getActivationCode());
            ps.setString(5, user.getActivationPathForActivationLink());
            ps.setBoolean(6, user.isActivated());
            ps.setString(7, user.getUsername());

            if (DbHelper.executeModify(ps) > 0)
                return true;

        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return false;
    }

    public static void activate(String username) {
        User user = findUserByLoginInDB(username);
        user.setAccountActivated();
        updateUserInDBbyUsername(user);
    }

    public static void setEmailAndDeactivate(String username, String email) {
        User user = findUserByLoginInDB(username);
        user.setEmailAndDeactivate(email);
        updateUserInDBbyUsername(user);
    }

}
