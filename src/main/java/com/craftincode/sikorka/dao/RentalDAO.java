package com.craftincode.sikorka.dao;

import com.craftincode.sikorka.model.Car;
import com.craftincode.sikorka.model.Rental;
import com.craftincode.sikorka.util.DateHelper;
import com.craftincode.sikorka.util.DbHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.craftincode.sikorka.util.DateHelper.getDateNoTime;
import static com.craftincode.sikorka.util.Log.error;
import static com.craftincode.sikorka.util.Log.say;

public class RentalDAO {

    public static final String COLUMN_ID = "rental_id";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_car_id = "car_id";
    public static final String COLUMN_date_started = "date_started";
    public static final String COLUMN_date_returned = "date_returned";
    public static final String COLUMN_days_to_rent = "days_to_rent";
    public static final String COLUMN_comment = "comment";
    public static final String COLUMN_price_paid = "price_paid";
    public static final String COLUMN_price_due = "price_due";
    public static final String COLUMN_PRICE_PER_DAY = "price_per_day";

    public static final String SELECT_ALL_RENTALS = "select * from carsrental.rental order by date_returned asc, date_started asc;";
    public static final String COUNT_RENTALS__BY_CAR_ID_AND_NOT_RETURNED =
            "select count(*) as count from carsrental.rental where car_id = ? and date_returned is null;";
    public static final String RENTALS__BY_CAR_ID_AND_NOT_RETURNED__OLDEST_FIRST =
            "select * from carsrental.rental where car_id = ? and date_returned is null order by date_started asc;"; //most past date first
    public static final String RENT_A_CAR =
            "insert into carsrental.rental\n" +
                "(username, car_id, date_started, days_to_rent, price_due, price_per_day)\n" +
                "VALUES (?, ?, ?, ?, ?, ?);";
    public static final String RENTALS__BY_USERNAME__FUTURE_FIRST =
            "select * from carsrental.rental where username = ? order by date_returned asc, date_started asc;"; //most past date first
    public static final String RENTAL__BY_ID =
            "select * from carsrental.rental where rental_id = ?;";
    public static final String RETURN_RENTAL =
            "update carsrental.rental\n" +
                    "SET date_returned = ?\n" +
                    "WHERE rental_id = ?";


    public static List<Rental> getAllRentals() {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, SELECT_ALL_RENTALS);

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();

                List<Rental> rentals = new ArrayList<Rental>();

                while (rs.next()) {
                    Rental user = createRentalFromResultSet(rs);
                    rentals.add(user);
                }

                return rentals;
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }

    private static Rental createRentalFromResultSet(ResultSet resultSet) {
        try {
            Integer id = resultSet.getInt(COLUMN_ID);
            String username = resultSet.getString(COLUMN_USERNAME);
            Integer carId = resultSet.getInt(COLUMN_car_id);
            Date dateStarted = resultSet.getDate(COLUMN_date_started);
            Date dateReturned = resultSet.getDate(COLUMN_date_returned);
            Integer daysToRent = resultSet.getInt(COLUMN_days_to_rent);
            String comment = resultSet.getString(COLUMN_comment);
            Double pricePaid = resultSet.getDouble(COLUMN_price_paid);
            Double priceDue = resultSet.getDouble(COLUMN_price_due);
            Double pricePerDay = resultSet.getDouble(COLUMN_PRICE_PER_DAY);

            return new Rental(id, username, carId, dateStarted, dateReturned, daysToRent, comment, pricePaid, priceDue, pricePerDay);
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong with result set data retrieval.");
        }

        return null;
    }

    public static boolean isCarRentedNow(Integer carId) {
        return getCurrentRentalForCar(carId) != null;
    }

    public static boolean isCarAvailableNow(int carId) {
        return !isCarRentedNow(carId);
    }

    public static Rental getCurrentRentalForCar(int carId) {
        List<Rental> notReturnedRentals = getNotReturnedRentalsForCarOldestDateFirst(carId);
        say("Not returned rentals for car " + carId + ": " + notReturnedRentals);

        for (int i=0; i<notReturnedRentals.size(); i++) {
            Rental rental = notReturnedRentals.get(i);
            say("Rental " + i + ": " + rental);

            if (rental != null && rental.isRentedNow()) {
                say("Found current rental: " + rental);
                return rental;
            }
        }

        return null;
    }

    public static List<Rental> getNotReturnedRentalsForCarOldestDateFirst(int carId) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, RENTALS__BY_CAR_ID_AND_NOT_RETURNED__OLDEST_FIRST);
            ps.setInt(1, carId);

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();

                List<Rental> rentals = new ArrayList<Rental>();
                while (rs.next()) {
                    Rental r = createRentalFromResultSet(rs);
                    rentals.add(r);
                }
                return rentals;
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }

    public static Date carRentedUntil(Car car) {
        if (car == null)
            return null;

        Rental currentRental = getCurrentRentalForCar(car.getId());

        if (currentRental == null)
            return null;

        return currentRental.dateToReturn();
    }

    public static Rental prepareNewRental(String username, int carId, java.util.Date fromDate, int days, double toPay, double pricePerDay) {
        return new Rental(null, username, carId, getDateNoTime(fromDate), null, days, null, 0, toPay, pricePerDay);
    }

    public static boolean isCarAvailableForRent(int carId, java.util.Date fromDate, int days) {
        List<Rental> rentalsForCar = getNotReturnedRentalsForCarOldestDateFirst(carId);

        Date toDate = DateHelper.datePlusDays(fromDate, days - 1);

        for (int i=0; i<rentalsForCar.size(); i++) {
            Rental rental = rentalsForCar.get(i);
            //if any rental overlaps the dates of current, or overdue, or future rentals
            if (rental.overlaps(fromDate, toDate))
                return false; //then car is not available for rent
        }

        return true;
    }

    /** This forcibly rents a car. Does not look if car is already rented or overlaps another rental or overdue. */
    public static int rentAcar(String username, Integer carId, java.util.Date fromDate, Integer numberOfDays, double toPay, double priceDaily) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        java.util.Date sqlDate = getDateNoTime(fromDate);

        say("Renting a car: " +
                        username + ", " +
                        carId + ", " +
                        sqlDate + ", " +
                        numberOfDays + ", " +
                        toPay + ", " +
                        priceDaily
                );

        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, RENT_A_CAR);
            ps.setString(1, username);
            ps.setInt(2, carId);
//            ps.setDate(3, sqlDate); //TODO BUG sets one day earlier in DB
            ps.setString(3, sqlDate.toString()); //workaround
            ps.setInt(4, numberOfDays);
            ps.setDouble(5, toPay);
            ps.setDouble(6, priceDaily);

            return DbHelper.executeModify(ps);
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement - got a null pointer exc :(");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return 0;
    }

    public static List<Rental> getRentalsForUser(String username) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, RENTALS__BY_USERNAME__FUTURE_FIRST);
            ps.setString(1, username);

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();

                List<Rental> rentals = new ArrayList<Rental>();
                while (rs.next()) {
                    Rental r = createRentalFromResultSet(rs);
                    rentals.add(r);
                }
                return rentals;
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }

    public static Rental getRental(Integer rentalId) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, RENTAL__BY_ID);
            ps.setInt(1, rentalId);

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();

                rs.next();

                return createRentalFromResultSet(rs);
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }

    public static int returnRental(Rental rental) {
        if (rental == null)
            return 0;

        rental.returnMe();

        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, RETURN_RENTAL);

            ps.setString(1, rental.getDateReturned().toString());
            ps.setInt(2, rental.getId());

            return DbHelper.executeModify(ps);

        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return 0;
    }
}
