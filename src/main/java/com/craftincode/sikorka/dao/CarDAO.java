package com.craftincode.sikorka.dao;

import com.craftincode.sikorka.model.Car;
import com.craftincode.sikorka.util.DbHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static com.craftincode.sikorka.util.Log.*;
import static com.craftincode.sikorka.util.TextHelper.*;

public class CarDAO {

    public static final String COLUMN_ID = "car_id";
    public static final String COLUMN_COLOR_HEX = "color_hex";
    public static final String COLUMN_SEATS = "seats";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_PROD_YEAR = "production_year";
    public static final String COLUMN_HORSE_POWER = "horse_power";
    public static final String COLUMN_PRICE_PER_DAY = "price_per_day";

    public static final String SELECT_ALL_CARS = "select * from carsrental.car";
    public static final String SELECT_CAR__BY_CAR_ID = "select * from carsrental.car where car_id = ?;";

    private static Set<String> availableCarColorsInHex;
    private static Set<String> availableCarBrands;
    private static Set<Integer> availableCarYears;

    static {
        recalculateAvailables(getAllCars());
    }

    public static List<Car> getAllCars(){
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, SELECT_ALL_CARS);

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();

                List<Car> cars = new ArrayList<Car>();

                while (rs.next()) {
                    Car car = createCarFromResultSet(rs);
                    if (car != null)
                        cars.add(car);
                }

                return cars;
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }

    private static Car createCarFromResultSet(ResultSet resultSet) {
        try {
            Integer id = resultSet.getInt(COLUMN_ID);
            String color = resultSet.getString(COLUMN_COLOR_HEX);
            Integer seats = resultSet.getInt(COLUMN_SEATS);
            String brand = resultSet.getString(COLUMN_BRAND);
            Integer prodYear = resultSet.getInt(COLUMN_PROD_YEAR);
            Integer horsePower = resultSet.getInt(COLUMN_HORSE_POWER);
            Double pricePerDay = resultSet.getDouble(COLUMN_PRICE_PER_DAY);

            return new Car(id, color, seats, brand, prodYear, horsePower, pricePerDay);
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong with result set data retrieval.");
        }

        return null;
    }

    public static List<Car> findCarsByBrandInList(Collection<Car> list, String brand) {
        if (brand == null || list==null)
            return null;

        return list.stream()
                .filter(car -> equalsIgnoreCase(brand, car.getBrand()))
                .collect(Collectors.toList());
    }

    public static List<Car> findCarsByYearInList(Collection<Car> list, Integer year) {
        if (year == null || list==null)
            return null;

        return list.stream()
                .filter(p -> year.equals(p.getProductionYear()))
                .collect(Collectors.toList());
    }

    public static List<Car> findCarsByColorInList(Collection<Car> list, String colorInHex) {
        if (colorInHex == null || list == null)
            return null;

        return list.stream()
                .filter(car -> equalsIgnoreCase(colorInHex, car.colorInHex()))
                .collect(Collectors.toList());
    }

    public static void addCar(Car car) {
        //TODO

        recalculateAvailables(getAllCars());
    }

    public static void removeCar(Car car) {
        //TODO

        recalculateAvailables(getAllCars());
    }

    private static void recalculateAvailables(List<Car> cars) {
        recalculateAvailableCarBrands(cars);
        recalculateAvailableCarColorsInHex(cars);
        recalculateAvailableCarProductionYears(cars);
    }

    public static Collection<String> getAvailableCarColorsInHex() {
        return availableCarColorsInHex;
    }

    private static void recalculateAvailableCarColorsInHex(List<Car> cars) {
        if (availableCarColorsInHex == null)
            availableCarColorsInHex = new HashSet<String>();
        else
            availableCarColorsInHex.clear();

        if (cars != null)
            for (Car car : cars)
                if (car != null) {
                    if (availableCarColorsInHex.add(car.colorInHex()))
                        say("Added color of a car: " + car);
                }
    }

    private static void recalculateAvailableCarBrands(List<Car> cars) {
        if (availableCarBrands == null)
            availableCarBrands = new HashSet<String>();
        else
            availableCarBrands.clear();

        if (cars != null)
            for (Car car : cars) {
                if (car != null) {
                    if (availableCarBrands.add(
                            car.getBrand()))
                        say("Added brand of a car: " + car);
                }
            }
    }

    public static Collection<String> getAvailableCarBrands() {
        return availableCarBrands;
    }

    private static void recalculateAvailableCarProductionYears(List<Car> cars) {
        if (availableCarYears == null)
            availableCarYears = new HashSet<Integer>();
        else
            availableCarYears.clear();

        if (cars != null)
            for (Car car : cars)
                if (car != null) {
                    if (availableCarYears.add(car.getProductionYear()))
                        say("Added production year of a car: " + car);
                }
    }

    public static Collection<Integer> getAvailableCarProductionYears() {
        return availableCarYears;
    }

    public static Car getCarById(int carId) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DbHelper.connect();
            ps = DbHelper.prepareStatement(connection, SELECT_CAR__BY_CAR_ID);
            ps.setInt(1, carId);

            if (DbHelper.executeSelect(ps)) {
                rs = ps.getResultSet();
                rs.next();

                return createCarFromResultSet(rs);
            };
        } catch (NullPointerException e) {
            e.printStackTrace();
            error("Something went wrong with prepped statement :/");
        } catch (SQLException e) {
            e.printStackTrace();
            error("Something went wrong in DB :/");
        } finally {
            DbHelper.close(rs);
            DbHelper.close(ps);
            DbHelper.close(connection);
        }

        return null;
    }
}
