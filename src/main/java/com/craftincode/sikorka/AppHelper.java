package com.craftincode.sikorka;

import com.craftincode.sikorka.dao.UserDAO;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.servlets.LoginServlet;
import com.craftincode.sikorka.util.Log;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;

import static com.craftincode.sikorka.util.Log.*;
import static com.craftincode.sikorka.util.TextHelper.*;

public class AppHelper {

    private static Properties emailProps = new Properties();
    private static Properties appProps = new Properties();

    static {
        Log.init();
        AppHelper.init();
    }

    public static final String SERVER_CONFIG_DIR = getAppProperty("SERVER_CONFIG_DIR");
    public static final String APP_NAME_IN_URL = getAppProperty("APP_NAME_IN_URL"); //"jeesmallapp-hi";
    public static final String APP_DOMAIN = getAppProperty("APP_DOMAIN");
    public static final String APP_PROTOCOL = getAppProperty("APP_PROTOCOL");

    public static final boolean DEBUG_ON = true; //Boolean.parseBoolean(getAppProperty("DEBUG_ON"));

    public static String getFileContentFromResources(String fileName) throws IOException {
        ClassLoader classLoader = AppHelper.class.getClassLoader();

        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            file = new File(resource.getFile());
        }

        FileReader reader = new FileReader(file);
        BufferedReader br = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        return sb.toString();
    }

    public static Cookie getCookieFromRequestByName(String cookieName, HttpServletRequest request) {
        if (request == null)
            return null; //TODO dobra praktyka: wyjatek czy null?

        return Arrays.stream(request.getCookies()).
                filter(p -> p.getName().equals(cookieName)).
                findFirst().
                orElse(null);
    }

    public static String getCookieValueFromRequestByName(String cookieName, HttpServletRequest request) {
        if (request == null)
            return null;

        return getCookieFromRequestByName(cookieName, request).getValue();
    }

    public static String getAppUrl() throws MalformedURLException {
        URL url = new URL(APP_PROTOCOL + "://" + APP_DOMAIN + "/" + APP_NAME_IN_URL + "/");

        return url.toString();
    }

    public static String getEmailProperty(String emailPropertyName) {
        if (emailProps == null) return null;

        return emailProps.getProperty(emailPropertyName);
    }

    private static String getAppProperty(String appPropertyName) {
        if (appProps == null) return null;

        return appProps.getProperty(appPropertyName);
    }

    public static void init() {
        String appPropsFile = "app.properties";
        try {
            appProps.load(AppHelper.class.getClassLoader().getResourceAsStream(appPropsFile));
            ok("Read " + appPropsFile);
            System.out.println(toJsonString(appProps));
        } catch (Exception e) {
            System.out.println("File '" + appPropsFile + "' was not found!");
            e.printStackTrace(); //TODO ??? czy moge abortowac apke jak propertiesy sie nie wczytaly? i jak?
        }

        String emailPropsFilePath = getAppProperty("SERVER_CONFIG_DIR") + "/email.properties";
        try {
            emailProps.load(new FileInputStream(emailPropsFilePath));
            ok("Read " + emailPropsFilePath);
            System.out.println(toJsonString(emailProps));
        } catch (Exception e) {
            System.out.println("File '" + emailPropsFilePath + "' was not found!");
            e.printStackTrace(); //TODO ??? czy moge abortowac apke jak propertiesy sie nie wczytaly? i jak?
        }
    }

    public static User getLoggedInUser(HttpSession session) {
        Object usernameInSessionOfLoggedInUser = session.getAttribute(LoginServlet.USERNAME_FORM_PARAM);
        if (usernameInSessionOfLoggedInUser == null)
            return null;

        if (!(usernameInSessionOfLoggedInUser instanceof String))
            return null;

        String username = (String) usernameInSessionOfLoggedInUser;
        if ("".equals(username))
            return null;

        return UserDAO.findUserByLoginInDB(username);
    }

}
