<%@ page import="com.craftincode.sikorka.servlets.admin.AdminUsersServlet" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.EditAccountByAdminServlet" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.dao.UserDAO" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Edit Account Page"/>
    <jsp:param name="styledir" value="../"/>
</jsp:include>
<body>
<jsp:include page="/includes/header.jsp"/>

<%

    String usernameToBeEdited = request.getParameter(EditAccountByAdminServlet.USERNAME_TO_BE_EDITED_FORM_PARAM);
    if (usernameToBeEdited == null) {
        HttpHelper.sendRedirectNOcontextPath(AdminUsersServlet.getMyJSP(), 400, response, request);

        return;
    }

    User userToBeEdited = UserDAO.findUserByLoginInDB(usernameToBeEdited);
    if (userToBeEdited == null) {
        HttpHelper.sendRedirectNOcontextPath(AdminUsersServlet.getMyJSP(), 400, response, request, EditAccountByAdminServlet.NO_USER_FOUND_TO_EDIT_URL_PARAM);

        return;
    }
%>

<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">
        <form method="post" action="<%=EditAccountByAdminServlet.getMyPath() %>">
            <div class="main-item">username: <%=userToBeEdited.getUsername() %>
                <input type="hidden" name="<%=EditAccountByAdminServlet.USERNAME_TO_BE_EDITED_FORM_PARAM%>"
                       value="<%=userToBeEdited.getUsername() %>"/></div>
            <div class="main-item">password: <input type="text"
                                                    name="<%=EditAccountByAdminServlet.PASSWORD_TO_BE_EDITED_FORM_PARAM%>"
                                                    value="<%=userToBeEdited.getPassword() %>"/></div>
            <div class="main-item">email: <input type="text"
                                                 name="<%=EditAccountByAdminServlet.EMAIL_TO_BE_EDITED_FORM_PARAM%>"
                                                 value="<%=userToBeEdited.getEmail() %>"/></div>
            <div class="main-item">activated: <%=userToBeEdited.isActivated() %>
            </div>
            <div class="main-item">is Admin: <%=userToBeEdited.isAdmin() %>
            </div>
            <div class="main-item">activation code: <%=userToBeEdited.getActivationCode() %>
            </div>
            <div class="main-item">activation path: <%=userToBeEdited.getActivationPathForActivationLink() %>
            </div>
            <div class="main-item"><input type="submit" value="Update user!" class="admin-input"></div>
        </form>
    </div>
</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>