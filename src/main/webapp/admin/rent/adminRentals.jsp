<%@ page import="com.craftincode.sikorka.AppHelper" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.dao.RentalDAO" %>
<%@ page import="com.craftincode.sikorka.model.Rental" %>
<%@ page import="java.util.List" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>
<%@ page import="com.craftincode.sikorka.servlets.LoginServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.rent.ReturnRentalByAdminServlet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Rental Page"/>
    <jsp:param name="styledir" value="../../"/>
</jsp:include>
<body>
<jsp:include page="/includes/header.jsp"/>

<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">

        <%
            User loggedInUser = AppHelper.getLoggedInUser(session);

            if (loggedInUser == null || !loggedInUser.isAdmin()) {
                HttpHelper.sendRedirectNOcontextPath(LoginServlet.getMyJSP(), 403, response, request); //user was logged out or removed by admin

                return;
            }

            Object exceptionObject = request.getAttribute(ReturnRentalByAdminServlet.EXCEPTION_MSG_ATTRIBUTE_NAME);
            if (exceptionObject != null) {
                String exceptionString = (String) exceptionObject;
        %>
        <div class="main-item-error"><%=exceptionString %></div>
        <%
            } else {
                Object successObject = request.getAttribute(ReturnRentalByAdminServlet.SUCCESS_MSG_ATTRIBUTE_NAME);
                if (successObject != null) {
                    String successString = (String) successObject;
        %>
        <div class="main-item-success"><%=successString %></div>
        <%
                }
            }

            List<Rental> allRentals = RentalDAO.getAllRentals();

            if (allRentals == null || allRentals.size() <= 0) {
        %>
            <div class="main-item-error">There are no rentals.</div>
        <%
            } else {
                for (Rental rental : allRentals) {
                    if (rental != null) {
        %>

        <jsp:include page="/includes/home/rent/rental.jsp">
            <jsp:param name="rentalJson" value="<%=rental.toJson()%>"/>
            <jsp:param name="isAdmin" value="<%=true %>"/>
        </jsp:include>

        <%
                    }
                }
            }
        %>

    </div>

</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>
