<%@ page import="com.craftincode.sikorka.servlets.LoginServlet" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.dao.UserDAO" %>
<%@ page import="com.craftincode.sikorka.servlets.home.HomePageServlet" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>
<%@ page import="java.util.List" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.DeleteAccountByAdminServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.EditAccountByAdminServlet" %>
<%@ page import="com.craftincode.sikorka.AppHelper" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Admin Page"/>
    <jsp:param name="styledir" value="../"/>
</jsp:include>
<body>
<jsp:include page="/includes/header.jsp"/>

<%
    User loggedInUser = AppHelper.getLoggedInUser(session);
    if (loggedInUser == null) {
        HttpHelper.sendRedirectNOcontextPath(LoginServlet.getMyJSP(), 403, response, request); //some unexpected shit here

        return;
    }

    if (!loggedInUser.isAdmin()) {
        HttpHelper.sendRedirectNOcontextPath(HomePageServlet.getMyJSP(), 403, response, request);

        return;
    }
%>

<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">
        <%
            if (request.getParameter(DeleteAccountByAdminServlet.WONT_DELETE_USER_URL_PARAM) != null) {
        %>
        <div class="main-item-error"><p><%=DeleteAccountByAdminServlet.WONT_DELETE_USER_MSG%>
        </p></div>
        <%
        } else if (request.getParameter(DeleteAccountByAdminServlet.NO_USER_FOUND_TO_DELETE_URL_PARAM) != null) {
        %>
        <div class="main-item-error"><p><%=DeleteAccountByAdminServlet.NO_USER_FOUND_TO_DELETE_MSG%>
        </p></div>
        <%
        } else if (request.getParameter(DeleteAccountByAdminServlet.USER_SUCCCESSFULLY_DELETED_URL_PARAM) != null) {
        %>
        <div class="main-item-success"><p><%=DeleteAccountByAdminServlet.USER_SUCCCESSFULLY_DELETED_MSG%>
        </p></div>
        <%
        } else if (request.getParameter(EditAccountByAdminServlet.NO_USER_FOUND_TO_EDIT_URL_PARAM) != null) {
        %>
        <div class="main-item-error"><p><%=EditAccountByAdminServlet.NO_USER_FOUND_TO_EDIT_MSG%>
        </p></div>
        <%
        } else if (request.getParameter(EditAccountByAdminServlet.USER_SUCCCESSFULLY_EDITED_URL_PARAM) != null) {
        %>
        <div class="main-item-success"><p><%=EditAccountByAdminServlet.USER_SUCCCESSFULLY_EDITED_MSG%>
        </p></div>
        <%
        } else if (request.getParameter(EditAccountByAdminServlet.USER_COULD_NOT_BE_SAVED_IN_DB_URL_PARAM) != null) {
        %>
        <div class="main-item-error"><p><%=EditAccountByAdminServlet.USER_COULD_NOT_BE_SAVED_IN_DB_MSG%>
        </p></div>
        <%
        } else if (request.getParameter(EditAccountByAdminServlet.EMAIL_CANT_BE_NULL_OR_EMPTY_URL_PARAM) != null) {
        %>
        <div class="main-item-error"><p><%=EditAccountByAdminServlet.EMAIL_CANT_BE_NULL_MSG%>
        </p></div>
        <%
        } else if (request.getParameter(EditAccountByAdminServlet.PASS_CANT_BE_NULL_URL_PARAM) != null) {
        %>
        <div class="main-item-error"><p><%=EditAccountByAdminServlet.PASS_CANT_BE_NULL_MSG%>
        </p></div>
        <%
            }

            List<User> users = UserDAO.getAllUsers();
            for (User u : users) {
        %>
        <div class="main-item">
            <jsp:include page="/includes/admin/user.jsp">
                <jsp:param name="userJson" value="<%=u.toJson()%>"/>
            </jsp:include>
        </div>
        <%
            }
        %>
    </div>
</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>