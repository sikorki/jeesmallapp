<%@ page import="com.craftincode.sikorka.servlets.admin.DeleteAccountByAdminServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.AdminUsersServlet" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Delete Account Confirmation Page"/>
    <jsp:param name="styledir" value="../"/>
</jsp:include>
<body>
<jsp:include page="/includes/header.jsp"/>

<%
    String usernameToBeDeleted = request.getParameter(DeleteAccountByAdminServlet.USERNAME_TO_BE_DELETED_URL_PARAM);
    if (usernameToBeDeleted == null) {
        HttpHelper.sendRedirectNOcontextPath(AdminUsersServlet.getMyJSP(), 400, response, request);

        return;
    }
%>

<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">
        <%
            if (request.getParameter(DeleteAccountByAdminServlet.WONT_DELETE_USER_URL_PARAM) != null) {
        %>
        <div class="main-item-error"><p><%=DeleteAccountByAdminServlet.WONT_DELETE_USER_MSG%></p></div>
        <%
        } else if (request.getParameter(DeleteAccountByAdminServlet.NO_USER_FOUND_TO_DELETE_URL_PARAM) != null) {
        %>
        <div class="main-item-error"><p><%=DeleteAccountByAdminServlet.NO_USER_FOUND_TO_DELETE_MSG%></p></div>
        <%
        } else if (request.getParameter(DeleteAccountByAdminServlet.USER_SUCCCESSFULLY_DELETED_URL_PARAM) != null) {
        %>
        <div class="main-item-success"><p><%=DeleteAccountByAdminServlet.USER_SUCCCESSFULLY_DELETED_MSG%></p></div>
        <%
        } else {
        %>
        <div class="main-item-warning">
            <form method="post" action="<%=DeleteAccountByAdminServlet.getMyPath() %>">
                <p>Are you sure you want to delete the
                    '<%=usernameToBeDeleted %>' account?</p>
                <input hidden type="text" name="<%=DeleteAccountByAdminServlet.USERNAME_TO_BE_DELETED_URL_PARAM %>"
                       value="<%=usernameToBeDeleted %>"/>
                <input type="submit" value="Yes, please" class="admin-input"/>
            </form>
            <a href="users.jsp">No no no, go back</a>
        </div>
        <%
            }
        %>
    </div>
</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>