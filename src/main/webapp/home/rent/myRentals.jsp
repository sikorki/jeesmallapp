<%@ page import="com.craftincode.sikorka.dao.UserDAO" %>
<%@ page import="com.craftincode.sikorka.AppHelper" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.dao.RentalDAO" %>
<%@ page import="com.craftincode.sikorka.model.Rental" %>
<%@ page import="java.util.List" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>
<%@ page import="com.craftincode.sikorka.servlets.LoginServlet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Rental Page"/>
    <jsp:param name="styledir" value="../../"/>
</jsp:include>
<body>
<jsp:include page="/includes/header.jsp"/>


<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">

        <%
            User loggedInUser = AppHelper.getLoggedInUser(session);

            if (loggedInUser == null) {
                HttpHelper.sendRedirectNOcontextPath(LoginServlet.getMyJSP(), 403, response, request); //user was logged out or removed by admin

                return;
            }

            List<Rental> rentalsForUser = RentalDAO.getRentalsForUser(loggedInUser.getUsername());

            if (rentalsForUser == null || rentalsForUser.size() <= 0) {
        %>
            <div class="main-item-error">There are no rentals for user.</div>
        <%
            } else {
                for (Rental rental : rentalsForUser) {
                    if (rental != null) {
        %>

        <jsp:include page="/includes/home/rent/rental.jsp">
            <jsp:param name="rentalJson" value="<%=rental.toJson()%>"/>
        </jsp:include>

        <%
                    }
                }
            }
        %>

    </div>

</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>
