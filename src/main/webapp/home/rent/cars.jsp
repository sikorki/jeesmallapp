<%@ page import="com.craftincode.sikorka.model.Car" %>
<%@ page import="com.craftincode.sikorka.dao.CarDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.craftincode.sikorka.util.TextHelper" %>
<%@ page import="java.util.Objects" %>
<%@ page import="com.craftincode.sikorka.util.Log" %>
<%@ page import="com.craftincode.sikorka.dao.RentalDAO" %>
<%@ page import="com.craftincode.sikorka.servlets.home.rent.PrepareToRentServlet" %>
<%@ page import="com.craftincode.sikorka.AppHelper" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.model.Rental" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Rental Page"/>
    <jsp:param name="styledir" value="../../"/>
</jsp:include>
<body>
<jsp:include page="/includes/header.jsp"/>


<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">
        <%
            List<Car> filteredListOdCars = CarDAO.getAllCars();

            Integer yearParamValue = null;
            String yearParamString = request.getParameter("year");
            if (yearParamString != null && !"".equals(yearParamString)) {
                try {
                    yearParamValue = Integer.valueOf(yearParamString);
                } catch (NumberFormatException e) { //BUG TODO non number values, suuport with err msg for user (?)
                    e.printStackTrace();
                    Log.error("Year was not a number!");
                }
            }

            String brandParamValue = request.getParameter("brand");

            String colorParamValue = request.getParameter("color");

            String clear = request.getParameter("clearSearch");
            if (clear == null) {

                out.print("<br/>Chosen color: " + colorParamValue);
                if (colorParamValue != null && !"".equals(colorParamValue)) {
                    filteredListOdCars = CarDAO.findCarsByColorInList(filteredListOdCars, colorParamValue);
                }

                out.print("<br/>Chosen brand: " + brandParamValue);
                if (brandParamValue != null && !"".equals(brandParamValue)) {
                    filteredListOdCars = CarDAO.findCarsByBrandInList(filteredListOdCars, brandParamValue);
                }

                out.print("<br/>Chosen year: " + yearParamValue);
                if (yearParamValue != null) {
                    filteredListOdCars = CarDAO.findCarsByYearInList(filteredListOdCars, yearParamValue);
                }
            }

            if (filteredListOdCars != null) {
                for (Car car : filteredListOdCars) {
                    if (car != null) {
                        Rental currentRental = RentalDAO.getCurrentRentalForCar(car.getId());
                        boolean rentedNow = currentRental != null;
        %>

    <% if (rentedNow) { %>
        <div class="main-item-dimmed">
    <% } else { %>
        <div class="main-item">
    <% } %>
            <jsp:include page="/includes/home/rent/car.jsp">
                <jsp:param name="carJson" value="<%=car.toJson() %>"/>
                <jsp:param name="rentalJson" value="<%=currentRental == null ? null : currentRental.toJson() %>"/>
            </jsp:include>
    <%
                        if (!rentedNow) { //car rented or overdue
                            User user = AppHelper.getLoggedInUser(session);
                            if (user != null && user.isActivated()) { //only if user is activated they can rent
    %>
            <form action="prepareToRent.jsp" method="get">
                <input type="hidden" name="<%=PrepareToRentServlet.CAR_ID_PARAM_NAME %>"
                       value="<%=car.getId() %>">
                <input type="submit" value="Rent">
            </form>
    <%
                            }
                        }
    %>
        </div>
    <%
                    }
                }
            }
        %>
    </div>

    <div class="search-area">
        <div class="search-item">
            <form name="search" method="get" action="cars.jsp">
                <select name="color">
                    <option value=""></option>
                    <%
                        Collection<String> availableCarColors = CarDAO.getAvailableCarColorsInHex();
                        if (availableCarColors != null)
                            for (String color : availableCarColors) {
                                if (color != null) {
                    %>
                    <option value="<%=color%>" <%
                        if (TextHelper.equalsIgnoreCase(color, colorParamValue))
                            out.print(" selected "); %>><%=color%>
                    </option>
                    <%
                                }
                            }
                    %>
                </select>
                <select name="brand">
                    <option value=""></option>
                    <%
                        Collection<String> availableBrands = CarDAO.getAvailableCarBrands();
                        if (availableBrands != null)
                            for (String brand : availableBrands) {
                                if (brand != null) {
                    %>
                    <option value="<%=brand%>" <%
                        if (TextHelper.equalsIgnoreCase(brand, brandParamValue))
                            out.print(" selected "); %>><%=brand%>
                    </option>
                    <%
                                }
                            }
                    %>
                </select>
                <select name="year">
                    <option value=""></option>
                    <%
                        Collection<Integer> availableProdYears = CarDAO.getAvailableCarProductionYears();
                        if (availableProdYears != null)
                            for (Integer year : availableProdYears) {
                                if (year != null) {
                    %>
                    <option value="<%=year%>" <%
                        if (Objects.equals(year, yearParamValue)) out.print(" selected "); %>><%=year%>
                    </option>
                    <%
                                }
                            }
                    %>
                </select>
                <input type="submit" value="Search!">
            </form>
        </div>
        <div class="search-item">
            <a href="cars.jsp?clearSearch">Clear Search</a>
        </div>
    </div>
</div>


<jsp:include page="/includes/footer.jsp"/>

</body>
</html>
