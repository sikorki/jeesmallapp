<%@ page import="com.craftincode.sikorka.model.Car" %>
<%@ page import="com.craftincode.sikorka.model.Rental" %>
<%@ page import="com.craftincode.sikorka.servlets.home.rent.CalculatePriceServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.home.rent.SubmitRental" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Prepare to Rent"/>
    <jsp:param name="styledir" value="../../"/>
</jsp:include>

<body>
<jsp:include page="/includes/header.jsp"/>

<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <%
        Object carObject = request.getAttribute("car");
        Car car = null;
        if (carObject instanceof Car) {
            car = (Car) carObject;
        } else {
            //TODO display error
            out.print("Car is null or not a car!");
            return;
        }

        Object rentalObject = request.getAttribute("rental");
        Rental rental = null;
        if (rentalObject instanceof Rental) {
            rental = (Rental) rentalObject;
        } else {
            //TODO display error
            out.print("Rental is null or not a rental!");
            return;
        }
    %>

    <div class="main-area">

    <% if (rental.isRentedNow()) { %>
        <div class="main-item-dimmed">
    <% } else { %>
        <div class="main-item">
    <% } %>
            <jsp:include page="/includes/home/rent/car.jsp">
                <jsp:param name="carJson" value="<%=car.toJson() %>"/>
            </jsp:include>
        </div>

        <div class="main-item-warning">
            <form method="post" action="<%=SubmitRental.getMyPath()%>">
                <p>Are you sure you want to rent the car?</p>
                <p>
                    total price: <span class="big"><%=rental.getPriceDue() %> PLN</span>
                    for <span class="big"><%=rental.getDaysToRent() %></span> days,
                    since <span class="big"><%=rental.getDateStarted() %></span>,
                    to <span class="big"><%=rental.dateToReturn() %></span>.
                </p>
                <input type="hidden" name="<%=CalculatePriceServlet.CAR_ID_FORM_PARAM_NAME%>" value="<%=car.getId()%>">
                <input type="hidden" name="<%=CalculatePriceServlet.NUMBER_OF_DAYS_FORM_PARAM_NAME%>" value="<%=rental.getDaysToRent()%>">
                <input type="hidden" name="<%=CalculatePriceServlet.FROM_DATE_FORM_PARAM_NAME%>" value="<%=rental.getDateStarted()%>">
                <input type="submit" value="YES!">
            </form>
        </div>
    </div>
</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>
