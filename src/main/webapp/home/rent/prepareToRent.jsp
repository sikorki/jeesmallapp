<%@ page import="com.craftincode.sikorka.model.Car" %>
<%@ page import="com.craftincode.sikorka.dao.CarDAO" %>
<%@ page import="com.craftincode.sikorka.dao.RentalDAO" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>
<%@ page import="com.craftincode.sikorka.util.DateHelper" %>
<%@ page import="com.craftincode.sikorka.servlets.home.rent.PrepareToRentServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.home.rent.CarsServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.home.rent.CalculatePriceServlet" %>
<%@ page import="com.craftincode.sikorka.model.Rental" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Prepare to Rent"/>
    <jsp:param name="styledir" value="../../"/>
</jsp:include>

<body>
<jsp:include page="/includes/header.jsp"/>

<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">
        <%
            boolean isNoError = true;

            Object exceptionObject = request.getAttribute(CalculatePriceServlet.EXCEPTION_MSG_ATTRIBUTE_NAME);
            if (exceptionObject != null) {
                String exceptionString = (String) exceptionObject;

                isNoError = false;
        %>
        <div class="main-item-error"><% out.print(exceptionString); %>
            <input type="button" value="Go Back" onclick="window.history.back()"/>
        </div>
        <%
            } else if (request.getParameter(PrepareToRentServlet.CAR_ID_NOT_CORRECT_URL_PARAM) != null) {
                isNoError = false;
        %>
        <div class="main-item-error"><% out.print(PrepareToRentServlet.CAR_ID_NOT_CORRECT_MSG); %>
            <input type="button" value="Go Back" onclick="window.history.back()"/>
        </div>
        <%
        } else if (request.getParameter(PrepareToRentServlet.CAR_NOT_FOUND_URL_PARAM) != null) {
            isNoError = false;
        %>
        <div class="main-item-error"><% out.print(PrepareToRentServlet.CAR_NOT_FOUND_MSG); %>
            <input type="button" value="Go Back" onclick="window.history.back()"/>
        </div>
        <%
            }
        %>

        <%
            if (isNoError) {
                String carIdString = request.getParameter(PrepareToRentServlet.CAR_ID_PARAM_NAME);
                int carId;

                try {
                    carId = Integer.parseInt(carIdString);
                } catch (Exception e) {
                    //TODO create param and msg
                    HttpHelper.sendRedirectNOcontextPath(PrepareToRentServlet.getMyJSP(), 400, response, request, PrepareToRentServlet.CAR_ID_NOT_CORRECT_URL_PARAM);

                    return;
                }

                Car car = CarDAO.getCarById(carId);

                if (car == null) {
                    HttpHelper.sendRedirectNOcontextPath(PrepareToRentServlet.getMyJSP(), 400, response, request, PrepareToRentServlet.CAR_NOT_FOUND_URL_PARAM);

                    return;
                } else {
                    Rental currentRental = RentalDAO.getCurrentRentalForCar(carId);
                    boolean rentedNow = currentRental != null;
        %>

        <% if (rentedNow) { %>
            <div class="main-item-dimmed">
        <% } else { %>
            <div class="main-item">
        <% } %>
                <jsp:include page="/includes/home/rent/car.jsp">
                    <jsp:param name="carJson" value="<%=car.toJson() %>"/>
                    <jsp:param name="rentalJson" value="<%=currentRental == null ? null : currentRental.toJson() %>"/>
                </jsp:include>
        <%
                    if (!rentedNow) { //car not rented now
        %>
                <form method="post" action="<%=CalculatePriceServlet.getMyPath()%>">
                    <div>Days to rent <input type="number" name="<%=CalculatePriceServlet.NUMBER_OF_DAYS_FORM_PARAM_NAME%>" min="1" required value="1"/></div>
                    <div>From date<input type="date" name="<%=CalculatePriceServlet.FROM_DATE_FORM_PARAM_NAME %>" pattern="2[0-9]{3}-[0-9]{2}-[0-9]{2}" required value="<%=DateHelper.tomorrow() %>"/></div>
                    <div><input type="hidden" name="<%=CalculatePriceServlet.CAR_ID_FORM_PARAM_NAME %>" value="<%=car.getId() %>"></div>
                    <input type="submit" value="Check Price and Availability"/>
                </form>
        <%
                    } //car not rented
        %>
            </div>
        <%
                } //car not null
            } //no error to display
        %>
    </div>
</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>
