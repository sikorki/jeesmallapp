<%@ page import="com.craftincode.sikorka.servlets.home.DeleteAccountByUserServlet" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Delete Account Confirmation Page"/>
    <jsp:param name="styledir" value="../"/>
</jsp:include>
<body>
<jsp:include page="/includes/header.jsp"/>

<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">
        <form method="post" action="<%=DeleteAccountByUserServlet.getMyPath() %>">
            <div class="main-item-warning">
                <p>Are you sure you want to delete your account?</p>
                <input type="submit" value="Yes, please">
            </div>
        </form>
    </div>
</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>
