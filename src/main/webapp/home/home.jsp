<%@ page import="com.craftincode.sikorka.servlets.LoginServlet" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.servlets.home.ActivateEmailServlet" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>
<%@ page import="com.craftincode.sikorka.servlets.SendActivationLinkServlet" %>
<%@ page import="com.craftincode.sikorka.AppHelper" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Home Page"/>
    <jsp:param name="styledir" value="../"/>
</jsp:include>
<body>

<jsp:include page="/includes/header.jsp"/>

<%
    User loggedInUser = AppHelper.getLoggedInUser(session);

    if (loggedInUser == null) {
        HttpHelper.sendRedirectNOcontextPath(LoginServlet.getMyJSP(), response, request); //some unexpected shit here

        return;
    }
%>

<div class="content">

    <jsp:include page="/includes/home/logged-in_menu.jsp"/>

    <div class="main-area">
        <%
            String messageSuccess = null;
            if (request.getParameter(ActivateEmailServlet.ACCOUNT_ACTIVATED_URL_PARAM) != null) {
                messageSuccess = ActivateEmailServlet.ACCOUNT_ACTIVATED_MSG;
            } else if (request.getParameter(SendActivationLinkServlet.ACTIVATION_EMAIL_SENT_URL_PARAM) != null) {
                messageSuccess = SendActivationLinkServlet.ACTIVATION_EMAIL_SENT_MSG;
            }

            if (messageSuccess != null) {
        %>
        <div class="main-item-success">
            <%
                out.print(messageSuccess);
            %>
        </div>
        <%
            } else {
                String message = null;
                if (request.getParameter(ActivateEmailServlet.WRONG_EMAIL_URL_PARAM) != null) {
                    message = ActivateEmailServlet.WRONG_EMAIL_MSG;
                } else if (request.getParameter(ActivateEmailServlet.MISSING_EMAIL_URL_PARAM) != null) {
                    message = ActivateEmailServlet.MISSING_EMAIL_MSG;
                } else if (request.getParameter(ActivateEmailServlet.WRONG_ACTIVATION_CODE_URL_PARAM) != null) {
                    message = ActivateEmailServlet.WRONG_ACTIVATION_CODE_MSG;
                } else if (request.getParameter(ActivateEmailServlet.MISSING_ACTIVATION_CODE_URL_PARAM) != null) {
                    message = ActivateEmailServlet.MISSING_ACTIVATION_CODE_MSG;
                }

                if (message != null) {
        %>
        <div class="main-item-error">
                <% out.print(message); %>
        </div>
        <%
                }
            }
        %>

        <div class="main-item">
            <p>Welcome <span class="big"><% out.print(loggedInUser.getUsername()); %></span>,</p>
        </div>

        <div class="main-item">
            <%
                if (!loggedInUser.isActivated()) {

                    //email was not sent yet
                    if (loggedInUser.getActivationCode() == null) {
                        out.print("Your account is not active ☝️ You need to activate it via email. ");
            %>

            <form method="post" action="sendActivationLink">
                Email: <input type="email" name="email">
                <input type="submit" value="Send activation email">
            </form>

            <%
            } else {
                out.print("Activation email was sent ✅ Click the link in email and you're done! <br/><br/>");
                out.print("Didn't get the email? 🚫 Type your email here:");
            %>

            <form method="post" action="sendActivationLink">
                Email: <input type="email" name="email" value="<%=loggedInUser.getEmail()%>">
                <input type="submit" value="Resend activation email">
            </form>

            <%
                    }
                } else {
                    out.print("Your account is active 👏");
                }
            %>
        </div>

        <div class="main-item">
            <form method="get" action="deleteAccountByUser.jsp">
                <input type="submit" value="Delete my account">
            </form>
        </div>
    </div>
</div>

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>