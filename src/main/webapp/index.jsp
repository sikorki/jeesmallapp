<%@ page import="com.craftincode.sikorka.servlets.*" %>
<%@ page import="com.craftincode.sikorka.AppHelper" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>
<%@ page import="com.craftincode.sikorka.servlets.home.HomePageServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.home.ActivateEmailServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.home.LogoutServlet" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Landing Page"/>
    <jsp:param name="styledir" value=""/>
</jsp:include>

<body>

<jsp:include page="/includes/header.jsp"/>

<div class="content">
    <div class="menu-area">
        <div class="menu-item">
            <%
                User user = AppHelper.getLoggedInUser(session);
                if (user != null) {
                    HttpHelper.sendRedirectNOcontextPath(HomePageServlet.getMyJSP(), 200, response, request);

                    return;
                }
            %>

            <form method="post" action="login">
                <p><input type="text" alt="Login" name="<% out.print(LoginServlet.USERNAME_FORM_PARAM); %>" autofocus/>
                </p>
                <p><input type="password" alt="Password" name="<% out.print(LoginServlet.PASSWORD_FORM_PARAM); %>"/></p>
                <input type="submit" name="login-user-button" value="Log me in">
            </form>

        </div>
        <div class="menu-item">
            <a href="register.jsp">Register Me!</a>
        </div>
    </div>

    <div class="main-area">
        <%
            String successMessage = null;
            if (request.getParameter(LogoutServlet.USER_LOGGED_OUT_URL_PARAM) != null) {
                successMessage = LogoutServlet.USER_LOGGED_OUT_MSG;
            } else if (request.getParameter(RegisterServlet.USER_ADDED_URL_PARAM) != null) {
                successMessage = RegisterServlet.USER_ADDED_MSG;
            }

            if (successMessage != null) {
        %>
        <div class="main-item-success"><%=successMessage %>
        </div>
        <%
        } else {
            String message = null;
            if (request.getParameter(LoginServlet.USER_DOES_NOT_EXIST_URL_PARAM) != null) {
                message = LoginServlet.USER_DOES_NOT_EXIST_MSG;
            } else if (request.getParameter(LoginServlet.BAD_PASS_URL_PARAM) != null) {
                message = LoginServlet.BAD_PASS_MSG;
            } else if (request.getParameter(LoginServlet.USER_NOT_LOGGED_IN_NO_SESSION_URL_PARAM) != null) {
                message = LoginServlet.USER_NOT_LOGGED_IN_NO_SESSION_MSG;
            } else if (request.getParameter(LoginServlet.USER_NOT_LOGGED_IN_SESSION_URL_PARAM) != null) {
                message = LoginServlet.USER_NOT_LOGGED_IN_SESSION_MSG;
            } else if (request.getParameter(LoginServlet.NO_USERNAME_PROVIDED_URL_PARAM) != null) {
                message = LoginServlet.NO_USERNAME_PROVIDED_MSG;
            } else if (request.getParameter(LoginServlet.NO_PASS_PROVIDED_URL_PARAM) != null) {
                message = LoginServlet.NO_PASS_PROVIDED_MSG;
            } else if (request.getParameter(LoginServlet.BAD_DATA_URL_PARAM) != null) {
                message = LoginServlet.BAD_DATA_MSG;
            }

            if (message != null) {
        %>
        <div class="main-item-error"><%= message %>
        </div>
        <%
        } else {
        %>
        <div class="main-item">
            Welcome to Pinkish Car Rental!
        </div>
        <%
                }
            }
        %>

        <%
            if (request.getParameter(ActivateEmailServlet.ACCOUNT_ACTIVATED_URL_PARAM) != null) {
        %>
        <div class="main-item-success">
            <%
                out.print(ActivateEmailServlet.ACCOUNT_ACTIVATED_MSG);
            %>
        </div>
        <%
        } else {
            String message = null;
            if (request.getParameter(ActivateEmailServlet.WRONG_EMAIL_URL_PARAM) != null) {
                message = ActivateEmailServlet.WRONG_EMAIL_MSG;
            } else if (request.getParameter(ActivateEmailServlet.MISSING_EMAIL_URL_PARAM) != null) {
                message = ActivateEmailServlet.MISSING_EMAIL_MSG;
            } else if (request.getParameter(ActivateEmailServlet.WRONG_ACTIVATION_CODE_URL_PARAM) != null) {
                message = ActivateEmailServlet.WRONG_ACTIVATION_CODE_MSG;
            } else if (request.getParameter(ActivateEmailServlet.MISSING_ACTIVATION_CODE_URL_PARAM) != null) {
                message = ActivateEmailServlet.MISSING_ACTIVATION_CODE_MSG;
            }

            if (message != null) {
        %>
        <div class="main-item-error">
            <% out.print(message); %>
        </div>
        <%
                }
            }
        %>

    </div>
</div>


<jsp:include page="/includes/footer.jsp"/>

</body>
</html>
