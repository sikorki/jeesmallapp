<%@ page import="com.craftincode.sikorka.model.Car" %>
<%@ page import="com.craftincode.sikorka.model.Rental" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    Car car = null;
    String carJson = request.getParameter("carJson");
    if (carJson != null)
        car = Car.fromJson(carJson);

    Rental currentRental = null;
    String rentalJson = request.getParameter("rentalJson");
    if (rentalJson != null) {
        currentRental = Rental.fromJson(rentalJson);
    }

    boolean rentedNow = false;
    boolean overdue = false;

    rentedNow = currentRental != null;

    if (currentRental != null)
        overdue = currentRental.isOverdue();
%>

    <div class="car-property">(<%=car.getId() %>) </div>
    <div class="car-color" style="background-color: <%=car.colorInHex() %>"></div>
    <div class="car-property big"><%=car.getBrand() %></div>,
    <div class="car-property"><%=car.getSeats() %> seats,</div>
    <div class="car-property">since <%=car.getProductionYear() %>,</div>
    <div class="car-property">power <%=car.getHorsePower() %>,</div>
    <div class="car-property">daily <%=car.getPricePerDay() %> PLN</div>
    <br/>

<% if (rentedNow) { %>
    <div class="car-property">
        <b>RENTED until the end of <%=currentRental.dateToReturn() %></b>
    </div>
<% } %>

<% if (overdue) { %>
    <div class="car-property">
        <b style="color: red">OVERDUE</b>
    </div>
<% } %>


