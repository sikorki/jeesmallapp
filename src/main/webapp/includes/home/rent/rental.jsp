<%@ page import="com.craftincode.sikorka.model.Car" %>
<%@ page import="com.craftincode.sikorka.model.Rental" %>
<%@ page import="com.craftincode.sikorka.dao.CarDAO" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.dao.UserDAO" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.rent.ReturnRentalByAdminServlet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%

    Rental rental = null;
    String rentalJson = request.getParameter("rentalJson");
    if (rentalJson != null) {
        rental = Rental.fromJson(rentalJson);
    }

    boolean isAdmin = Boolean.parseBoolean(request.getParameter("isAdmin"));

    if (rental != null) {
        boolean rentedNow = false;
        boolean overdue = false;
        boolean futureRental = false;
        boolean pastRental = false;

        overdue = rental.isOverdue();
        rentedNow = rental.isRentedNow();
        futureRental = rental.isFuture();
        pastRental = rental.isPast();

        if (rentedNow) {
%>
<div class="main-item-success">
<%
        } else if (futureRental) {
%>
<div class="main-item">
<%
        } else if (pastRental) {
%>
<div class="main-item-dimmed">
<%
        } else if (overdue) {
%>
<div class="main-item-warning">
<%
        }
%>
    <div class="car-property">(<%=rental.getId() %>) </div>
    <div class="car-property">start date <%=rental.getDateStarted() %>,</div>
    <div class="car-property">days to rent <%=rental.getDaysToRent() %>,</div>
    <div class="car-property">date to return <%=rental.dateToReturn() %>,</div><br/>
<%
    if (rental.isReturned()) {
        out.print("RETURNED on " + rental.getDateReturned());
    }

    if (isAdmin) {
        if (!rental.isReturned() && !rental.isFuture()) {
%>
        <form method="post" action="<%=ReturnRentalByAdminServlet.getMyPath() %>">
            <input type="hidden" name="rentalId" value="<%=rental.getId()%>">
            <input type="submit" value="Return" class="admin-input">
        </form>
<%
        }
    }
%>
    <div class="car-property">price paid <%=rental.getPricePaid() %> PLN,</div>
    <div class="car-property">price due <%=rental.getPriceDue() %> PLN,</div>
    <br/>

<% if (rentedNow) { %>
    <div class="car-property">
        <b>RENTED until the end of <%=rental.dateToReturn() %></b>
    </div>
<% } %>

<% if (overdue) { %>
    <div class="car-property">
        <b style="color: red">OVERDUE</b>
    </div>
<% } %>

<% if (pastRental) { %>
<div class="car-property">
    <b style="color: gray">PAST RENTAL</b>
</div>
<% } %>

<% if (futureRental) { %>
<div class="car-property">
    <b>FUTURE RENTAL</b>
</div>
<% } %>

    <%
        Car car = CarDAO.getCarById(rental.getCarId());
        if (car != null) {
    %>
    <div class="main-item" style="margin-left: 150px">
        <jsp:include page="car.jsp">
            <jsp:param name="carJson" value="<%=car.toJson()%>"/>
        </jsp:include>
    </div>
    <%
        }
    %>

    <%
        if (isAdmin) {
            User user = UserDAO.findUserByLoginInDB(rental.getUsername());
            if (user != null) {
    %>
    <div class="main-item" style="margin-left: 150px">
        <jsp:include page="../../admin/user.jsp">
            <jsp:param name="userJson" value="<%=user.toJson()%>"/>
        </jsp:include>
    </div>
    <%
            }
        }
    %>
</div>

<%
    } //if (rental != null)
%>