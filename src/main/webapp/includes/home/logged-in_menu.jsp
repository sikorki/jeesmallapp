<%@ page import="com.craftincode.sikorka.servlets.LoginServlet" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.AppHelper" %>
<%@ page import="com.craftincode.sikorka.util.HttpHelper" %>
<%@ page import="com.craftincode.sikorka.servlets.home.HomePageServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.home.rent.CarsServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.AdminUsersServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.home.rent.MyRentalsServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.rent.AdminRentalsServlet" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    User user = AppHelper.getLoggedInUser(session);

    if (user == null) {
        HttpHelper.sendRedirectNOcontextPath(LoginServlet.getMyJSP(), response, request); //some unexpected shit here

        return;
    }
%>

<jsp:include page="/includes/flying_bubble.jsp"/>

<div class="menu-area">
    <div class="menu-item">
        <a href="<%=HomePageServlet.getMyJSP()%>">Profile</a>
    </div>
    <div class="menu-item">
        <a href="<%=CarsServlet.getMyJSP()%>">Cars to Rent</a>
    </div>
    <div class="menu-item">
        <a href="<%=MyRentalsServlet.getMyJSP()%>">My Rentals</a>
    </div>

    <%
        if (user.isAdmin()) {
    %>
    <div class="menu-area-admin">
        <div class="menu-item-admin">
            <a href="<%=AdminUsersServlet.getMyJSP()%>">Users</a>
        </div>
        <div class="menu-item-admin">
            <a href="<%=AdminRentalsServlet.getMyJSP()%>">Rentals</a>
        </div>
    </div>
    <%
        }
    %>

</div>

