<%@ page import="com.craftincode.sikorka.servlets.home.LogoutServlet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="flying-bubble">
    <form method="get" action="<%=LogoutServlet.getMyPath()%>">
        <input type="submit" name="logout-user-button" value="Logout" class="flying-bubble-input">
    </form>
</div>