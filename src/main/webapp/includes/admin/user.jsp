<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.DeleteAccountByAdminServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.admin.EditAccountByAdminServlet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    User user = null;
    String userJson = request.getParameter("userJson");
    if (userJson != null) {
        user = User.fromJson(userJson);
%>

<div class="car-property big"><%=user.getUsername() %></div>,
<div class="car-property">pass:
    <%
        if (user.getPassword() == null)
            out.print("is NULL");
        else if ("".equals(user.getPassword()))
            out.print("is EMPTY");
        else out.print(user.getPassword());
    %>, </div>
<div class="car-property">email: <%=user.getEmail() %>, </div>
<div class="car-property"><%=user.getRole() %>, </div>
<div class="car-property">
    <%
        if (user.isActivated())
            out.print("ACTIVATED");
        else
            out.print("NOT ACTIVATED");
    %>
</div>
<div class="car-property">activation code: <%=user.getActivationCode() %>, </div>
<div class="car-property">activation link: <%=user.getActivationPathForActivationLink() %></div>

<form method="post" action="<%=DeleteAccountByAdminServlet.getMyJSP()%>?usernameToBeDeleted=<%=user.getUsername() %>">
    <input type="submit" value="Delete" class="admin-input">
</form>
<form method="post" action="<%=EditAccountByAdminServlet.getMyJSP()%>?usernameToBeEdited=<%=user.getUsername() %>">
    <input type="submit" value="Edit" class="admin-input">
</form>

<%
    }
%>