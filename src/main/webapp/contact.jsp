<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Contact Us"/>
    <jsp:param name="styledir" value=""/>
</jsp:include>

<body>

<jsp:include page="/includes/header.jsp"/>

Some contact, someday.

<jsp:include page="/includes/footer.jsp"/>

</body>
</html>
