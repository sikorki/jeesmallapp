<%@ page import="com.craftincode.sikorka.servlets.LoginServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.home.HomePageServlet" %>
<%@ page import="com.craftincode.sikorka.servlets.RegisterServlet" %>
<%@ page import="com.craftincode.sikorka.model.User" %>
<%@ page import="com.craftincode.sikorka.AppHelper" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<jsp:include page="/includes/head.jsp">
    <jsp:param name="pagetitle" value="Registration Page"/>
    <jsp:param name="styledir" value=""/>
</jsp:include>
<body>
<jsp:include page="/includes/header.jsp"/>

<div class="content">

    <div class="menu-area">
        <div class="menu-item">
            <%
                User user = AppHelper.getLoggedInUser(session);
                if (user != null) {
                    response.sendRedirect(HomePageServlet.MY_JSP);

                    return; //not needed but clearer
                }
            %>

            <form method="post" action="login">
                <p><input type="text" alt="Login" name="<% out.print(LoginServlet.USERNAME_FORM_PARAM); %>" autofocus/>
                </p>
                <p><input type="password" alt="Password" name="<% out.print(LoginServlet.PASSWORD_FORM_PARAM); %>"/></p>
                <input type="submit" value="Log me in">
            </form>

        </div>
        <div class="menu-item">
            <a href="register.jsp">Register Me!</a>
        </div>
    </div>

    <div class="main-area">
        <%
            Object exceptionMsgObject = request.getAttribute(RegisterServlet.EXCEPTION_MSG_ATTRIBUTE);
            if (exceptionMsgObject != null) {
                String exceptionMsgString = (String) exceptionMsgObject;
        %>
        <div class="main-item-error">
            <%
                out.print(exceptionMsgString);
            %>
        </div>
        <%
            }
        %>
        <div class="main-item">
            <form method="post" action="register">
                <div>Login <input type="text" name="<% out.print(LoginServlet.USERNAME_FORM_PARAM); %>" autofocus/>
                </div>
                <div>Pass <input type="text" name="<% out.print(LoginServlet.PASSWORD_FORM_PARAM); %>"/></div>
                <input type="submit" name="register-user-button" value="Register me">
            </form>
        </div>
    </div>

</div>

<jsp:include page="/includes/footer.jsp"/>
</body>
</html>