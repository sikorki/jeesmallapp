package com.craftincode.sikorka.model;

import com.craftincode.sikorka.category.level.Unit;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@Category(Unit.class)
public class UserTest {

    @Test
    public void whenComparyingUsersOfSameLoginButNoEmailSet_trueReturned() {
        User user1 = new User("anIa", "bania");
        User user2 = new User("Ania", "bania");

        assertThat("users are equal", user1, is(equalTo(user2)));
    }

    @Test
    public void whenComparyingUsersOfSameLoginAndEmail_falseReturned() {
        User user1 = new User("anIa", "bania");
        user1.setEmailAndDeactivate("bb@cc.pl");
        System.out.println(user1);

        User user2 = new User("Ania", "bania");
        user2.setEmailAndDeactivate("bb@cc.pl");
        System.out.println(user2);

        assertThat("users are not equal", user1, is(not(equalTo(user2))));
    }

    @Test
    public void whenActivatingUser_thenCorrectReturned() {
        User user = new User("ktos", "tam");
        assertThat("new user should not be activated",
                user.isActivated(),
                is(false));
        System.out.println(user);

        user.setAccountActivated();
        assertThat("after setting account active user should be activated",
                user.isActivated(),
                is(true));
        System.out.println(user);

        user.setEmailAndDeactivate("aa@bb.cc");
        assertThat("after setting new email user should not be active",
                user.isActivated(),
                is(false));
        System.out.println(user);
    }

}
