package com.craftincode.sikorka.model;

import com.craftincode.sikorka.category.level.Unit;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.awt.*;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static com.craftincode.sikorka.util.Util.*;

@Category(Unit.class)
public class CarTest {

    @Test
    public void whenCarsOfSamePropsAreCompared_thenEqual() {
        Car car1 = new Car(1, colorToHex(Color.BLUE), 2, "VOLVO", 1990, 100, 101.2);
        Car car2 = new Car(2, colorToHex(Color.BLUE), 2, "volvo", 1990, 100, 101.2);

        assertThat("car1 and car2 should be considered equal", car1, is(equalTo(car2)));
    }

    @Test
    public void whenCarsOfDifferentPropsAreCompared_thenNotEqual() {
        Car car1 = new Car(3, Color.BLUE, 2, Car.Brand.VOLVO, 1990, 100, 100.2);
        Car car2 = new Car(4, Color.BLUE, 2, Car.Brand.BMW, 1990, 100, 100.2);
        assertThat("car1 and car2 are not equal", car1, is(not(equalTo(car2))));
        System.out.println(car1);
        System.out.println(car2);

        car2.setBrand(Car.Brand.VOLVO);
        car2.setSeats(4);
        assertThat("car1 and car2 are not equal", car1, is(not(equalTo(car2))));

        car2.setSeats(2);
        car2.setColor(Color.YELLOW);
        assertThat("car1 and car2 are not equal", car1, is(not(equalTo(car2))));

        car2.setColor(Color.BLUE);
        car2.setProductionYear(1980);
        assertThat("car1 and car2 are not equal", car1, is(not(equalTo(car2))));

        car2.setProductionYear(1990);
        assertThat("car1 and car2 are equal", car1, is(equalTo(car2)));

        car2.setHorsePower(200);
        assertThat("car1 and car2 are not equal", car1, is(not(equalTo(car2))));

        car2.setHorsePower(100);
        assertThat("car1 and car2 are not equal", car1, is(equalTo(car2)));

        car2.setPricePerDay(200);
        assertThat("car1 and car2 are not equal", car1, is(not(equalTo(car2))));

        car2.setPricePerDay(100.2);
        assertThat("car1 and car2 are equal", car1, is(equalTo(car2)));
    }

}
