package com.craftincode.sikorka.dao;

import com.craftincode.sikorka.category.kind.DBtest;
import com.craftincode.sikorka.category.level.Integration;
import com.craftincode.sikorka.model.User;
import com.craftincode.sikorka.util.DbHelper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static com.craftincode.sikorka.dao.UserDAO.*;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@Category({Integration.class, DBtest.class})
public class UserDAOTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    static List<String> users = new ArrayList<String>();

    @BeforeClass
    public static void setUp() throws Exception {
        addNewUserForTestWithAnyData("activated", "activated");
        User user = findUserByLoginInDB("activated");
        UserDAO.activate(user.getUsername());

        addNewUserForTestWithAnyData("notactivated", "");

        addNewUserForTestWithAnyData("adminThatDoesNotComplyWithLoginRegex_forTests", "aDmin_%!"); //bad username/pass - old validation
    }

    static boolean addNewUserForTestWithAnyData(String username, String password) throws Exception {
        users.add(username);

        Connection connection = DbHelper.connect();
        String sql = "INSERT INTO carsrental.user (username, password) VALUES ('" + username + "', '" + password + "');";
        System.out.println(sql);

        boolean allGood = connection.createStatement().execute(sql);
        connection.close();

        return allGood;
    }

    static boolean addNewUser(String username, String pass) throws Exception {
        boolean added = UserDAO.addNewUser(username, pass);

        if (added)
            users.add(username);

        return added;
    }

    @AfterClass
    public static void tearDown() {
        System.out.println(users);

        //remove all test users
        for (String username: users) {
            deleteAccountInDBbyUsername(username);
        }
    }

    @Test
    public void whenAddUser_withCorrectData_thenUserAdded() throws Exception {
        assertTrue(addNewUser("mnft", "bania"));
        User user = findUserByLoginInDB("mnft");
        assertEquals(user.getUsername(), "mnft");
        assertEquals(user.getPassword(), "bania");

        assertTrue(addNewUser("nnnn", ""));
        user = findUserByLoginInDB("nnnn");
        assertEquals(user.getUsername(), "nnnn");
        assertEquals(user.getPassword(), "");

        assertTrue(addNewUser("ALe", ""));
        user = findUserByLoginInDB("ale");
        assertEquals(user.getUsername(), "ale");
        assertEquals(user.getPassword(), "");

        assertTrue(EXC_PASS_CAN_ONLY_HAVE_LETTERS_AND_NUMBERS, addNewUser("usera", "abcdefghijklmnopqrstuvxyz"));
        assertTrue(EXC_PASS_CAN_ONLY_HAVE_LETTERS_AND_NUMBERS, addNewUser("userb", "ABCDEFGHIJKLMNOPQRSTUVXYZ"));
        assertTrue(EXC_PASS_CAN_ONLY_HAVE_LETTERS_AND_NUMBERS, addNewUser("userc", "0123456789"));
    }

    @Test
    public void whenAddUser_withBigLettersInUsername_thenUserAddedWithSmallLettersInUsername() throws Exception {
        assertThat("adding new user with big letters in username is a success",
                addNewUser("AlEf", "Ab1"),
                is(true));

        User user = findUserByLoginInDB("AlEf");
        System.out.println(user);

        assertThat("user added with big letters in username has username saved with small letters",
                user.getUsername(),
                is(equalTo("alef")));
        assertThat("user found has correct password",
                user.getPassword(),
                is(equalTo("Ab1")));
    }

    @Test
    public void whenAddDefaultUserAgain_thenExceptionAlreadyExists() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_USER_ALREADY_EXISTS);
        addNewUser("activated", "activated");
    }

    @Test
    public void whenAddUser_andAddAgain_thenExceptionAlreadyExists() throws Exception {
        assertTrue(addNewUser("AbC", "AbC"));
        System.out.println(users);

        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_USER_ALREADY_EXISTS);
        addNewUser("AbC", "AbC1");
        System.out.println(users);
    }

    @Test
    public void whenAddUser_withTooLongPass_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_TOO_LONG_PASS);
        addNewUser("userx", "aabbccddeeaabbccddeeaabbccddeex");
    }

    @Test
    public void whenAddUser_withNullPass_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_PASS_CANT_BE_NULL);
        addNewUser("an", null);
    }

    @Test
    public void whenAddUser_withNullPassEvenThoughTooShortUsername_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_PASS_CANT_BE_NULL);
        addNewUser("a", null);
    }

    @Test
    public void whenAddUserWithTooShortUsername_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_TOO_SHORT_USERNAME);
        addNewUser("a", "");
    }

    @Test
    public void whenAddUserWithEmptyUsername_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_USERNAME_CANT_BE_EMPTY_STRING);
        addNewUser("", "");
    }

    @Test
    public void whenAddUserWithNullUsername_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_USERNAME_CANT_BE_NULL);
        addNewUser(null, "");
    }

    @Test
    public void whenAddUserWithTooLongUsername_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_TOO_LONG_USERNAME);
        addNewUser("aabbccddeeaabbccddeeaabbccddeex", "");
    }

    @Test
    public void whenAddUser_withUsernameWithNumbers_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_USERNAME_CAN_ONLY_HAVE_LETTERS);
        addNewUser("abcdefghijklmnopqrstuvxyz2", "");
    }

    @Test
    public void whenAddUser_withUsernameWithAdot_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_USERNAME_CAN_ONLY_HAVE_LETTERS);
        addNewUser("abcdefghijklmnopqrstuvxyz.", "");
    }

    @Test
    public void whenAddUser_withPassWithAdot_thenCorrectException() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage(EXC_PASS_CAN_ONLY_HAVE_LETTERS_AND_NUMBERS);
        addNewUser("userd", "abcdefghijklmnRSTUVXYZ0123456.");
    }

    @Test
    public void whenGetPasswordForUsername_thenCorrectReturned() {
        System.out.println(users);
        assertThat("password for username that has big letters is correct",
                getPasswordForUsername("adminThatDoesNotComplyWithLoginRegex_forTests"),
                is(equalTo("aDmin_%!")));

        assertThat("password for username that has big letters is correct",
                getPasswordForUsername("adminthatdoesnotcomplywithloginregex_fortests"),
                is(equalTo("aDmin_%!")));

        assertThat("password for username=null is null", getPasswordForUsername(null), is(nullValue()));
        assertThat("password for username=\"\" is null", getPasswordForUsername(""), is(nullValue()));
    }

    @Test
    public void whenCheckingActivationFlag_thenCorrectReturned() {
        assertTrue(findUserByLoginInDB("activated").isActivated());
        assertFalse(findUserByLoginInDB("notactivated").isActivated());
    }

    @Test
    public void givenUserAdded_whenDeleteUserAccount_thenUserDoesNotExist() throws Exception {
        addNewUser("userToBeDeleted", "dumpass");
        User user = findUserByLoginInDB("userToBeDeleted");

        deleteAccountInDBbyUsername(user);

        assertThat("user should not exist anymore",
                exists("userToBeDeleted"),
                is(false));
    }

}
